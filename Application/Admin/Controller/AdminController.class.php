<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class AdminController extends AdminBaseController  {
    protected $_model='Admin';
    //关联查询所有对index方法重写
    public function index(){
        $admin = D('AdminView'); // 实例化Data数据对象
        $data = $admin->getAdminData();
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display(); // 输出模板
    }
//有后置操作，所以对del方法重写，覆盖继承的BaseController的方法
    public function del(){
        $admin=D('Admin');
        $data=$admin->delAdminById();
        $this->ajaxReturn(array(
            'status' => $data['status'],
            'info' => $data['info'],
        ));
    }
    /*
     * 分配角色
     */
    public function add_role(){
        if(IS_POST){
            $role=D('AuthGroupAccess');
            $data=$role->addAuthGroupAccess();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }else{
            $admin=D('Admin');
            $admin_res=$admin->find(I('id'));
            $authGroup=D('AuthGroup');
            $authGroup_res=$authGroup->getAllData();
            $assign=array(
                'data'=>$admin_res,
                'authGroup'=>$authGroup_res,
            );
            $this->assign($assign);
        }
        $this->display();
    }
    /*
     * 编辑角色
     */
    public function edit_role(){
        if(IS_POST){
            $role=D('AuthGroupAccess');
            $data=$role->editAuthGroupAccess();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }else{
            $admin=D('Admin');
            $admin_res=$admin->find(I('id'));
            $authGroup=D('AuthGroup');
            $authGroup_res=$authGroup->getAllData();
            $assign=array(
                'data'=>$admin_res,
                'authGroup'=>$authGroup_res,
            );
            $this->assign($assign);
        }
        $this->display();
    }
    public function logout(){
        session(null);
        //$_SESSION['admin']=null;
        $this->success("退出成功",U('Login/index'));
    }


}