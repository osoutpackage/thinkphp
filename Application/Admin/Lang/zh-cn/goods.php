<?php

/**
 * 这个是Admin模块的Goods控制器的语言包，仅限Goods控制器引用的，其他控制器无法使用。
 * lingda的 简体中文语言包
 */
return array(
    /* 语言变量 */
    'goods_success'     => '操作商品成功',
    'goods_error'         => '操作商品失败',


);
