<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Model;
use Common\Model\BaseModel;

class AuthGroupAccessModel extends BaseModel  {
    protected $_validate = array(
        array('uid','require','用户id不能为空！',0,'regex',3),
        array('group_id','require','角色id不能为空！',0,'regex',3),
    );
    public function addAuthGroupAccess(){
        //设置一个管理员只能属于拥有一个角色
        if($this->where('uid = '.I('uid'))->find()){
            return array(
                'status' => 'error',
                'info' => '分配失败，该管理员已经拥有角色！',
            );
        }
        $result=$this->addData();//调用父类的添加方法 parent::addData()
        return array(
            'status' => $result['status'],
            'info' => $result['info'],
        );
    }
    public function editAuthGroupAccess(){
        if($this->create()){
            if(false!==$this->where('uid='.I('uid'))->save()){
                return array(
                    'status' => 'success',
                    'info' => '修改成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '修改失败！',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }
    }

}