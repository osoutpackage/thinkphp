<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class UserController extends AdminBaseController
{
    protected $_model='User';
    protected function _where(){
        return array('status'=>1);
        //return 'status = 1';
    }

    public function del(){
        $user=D('User');
        $data=$user->delUserData();
        $this->ajaxReturn(array(
            'status' => $data['status'],
            'info' => $data['info'],
        ));
    }
}