<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class GoodsOrderController extends AdminBaseController  {
    protected $_model='GoodsOrder';
    public function index(){
        $model = D('GoodsOrderView');
        $data = $model->getGoodsOrderData();
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }


    /*
     * 批量删除商品
     */
    public function bDel(){
        $ids_arr=I('id');
        if($ids_arr){
            $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
            $order=D('GoodsOrder');
            if($order->delete($ids)){
                $this->success('批量删除成功',U('index'));
            }else{
                $this->error('批量删除失败');
            }
        }else{
            $this->error('未选择有数据');
        }
    }





}