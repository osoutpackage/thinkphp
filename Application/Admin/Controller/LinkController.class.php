<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class LinkController extends AdminBaseController  {
    protected $_model='Link';
    protected $_order='sort asc';

    public function add(){
        if(IS_POST){
            $link = D('Link');
            $data = $link->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $this->display();
    }
    public function edit(){
        $link=D('Link');
        if(IS_POST){
            $data = $link->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $link_res=$link->find(I('id'));
            $this->assign('data',$link_res);
        }
        $this->display();
    }


}