<?php
namespace Common\Model;
use Common\Model\BaseModel;

class CateModel extends BaseModel  {
        protected $_validate = array(
        array('catename','require','添加商品分类不能为空！',1,'regex',3), // 必须有
        array('catename','','添加商品分类不得重复！',1,'unique',3), // 验证是否已经存在
    );
        protected $_auto = array (
            array('addtime','time',1,'function'),
            array('is_show','getShow',3,'callback'),
        );
        public function getShow(){
            if(I('is_show')){
                $is_show=1;
            }else{
                $is_show=0;//为空的时候设置为0，否则会被认为不修改。
            }
            return $is_show;
        }



        /**
         **通过父分类获取其下的子分类
        **/
        public function chilrenid($cateid){
            $data=$this->select();
            return $this->getchilrenid($data,$cateid);
        }
        public function getchilrenid($data,$parentid){
            static $ret=array();
            foreach($data as $k=>$v){
                if($v['pid']==$parentid){
                    $ret[]=$v['id'];
                    $this->getchilrenid($data,$v['id']);
                }
            }
            return $ret;
        }
        //钩子函数，执行delete前的操作
        public function _before_delete($options){
            //print_r($options);die();
            if(is_array($options['where']['id'])){//判断如果是数组则是执行del_all批量删除
                $arr=explode(',',$options['where']['id'][1]);//$options['where']['id'][1]得到是逗号分隔的id数，要转化为数组
                //print_r($arr);die();
                $soncates=array();
                foreach($arr as $k=>$v){
                    $soncates2=$this->chilrenid($v);
                    $soncates=array_merge($soncates,$soncates2);//数组合并，把得到的soncates2赋值到soncates数组里
                    //$soncates[]=$this->chilrenid($v);
                }
                //print_r($soncates);
                $soncates=array_unique($soncates);//array_unique去重，把数组里重复的id去除，防止重新删除的低效；
                $soncates=implode(',',$soncates);
                if($soncates){
                    $this->execute("delete from ".$options['table']." where id in ($soncates)");
                }
            }else{//否则是执行del单个删除，删除前先把其下的子分类全部查找并且删除
                $chilrenid_arr=$this->chilrenid($options['where']['id']);
                $chilrenids=implode(',',$chilrenid_arr);
                if($chilrenids){
                    $this->execute("delete from ".$options['table']." where id in ($chilrenids)");
                }

            }
        }

}