<?php
return array(
    //'配置项'=>'配置值'

    'SESSION_OPTIONS'         =>  array(
        'name'                =>  'lingdaadmin',                    //设置session名
        'prefix'              =>  'admin_',                     // 设置前缀 避免跟home模块的冲突
        'expire'              =>  3*60*60,                        //SESSION保存时间
        'use_trans_sid'       =>  1,                               //跨页传递
        //'use_only_cookies'    =>  0,                               //是否只开启基于cookies的session的会话方式

    ),
);