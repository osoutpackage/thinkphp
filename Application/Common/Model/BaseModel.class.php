<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Think\Model;
class BaseModel extends Model {
    //自动验证表单
    protected $_validate = array(
        array('verify','check_verify','验证码不正确！',0,'callback',3),
        array('verify','check_verify','验证码不正确！',0,'callback',4),//登录验证码

    );
    public function check_verify($code, $id = ''){
        $verify = new \Think\Verify();
        return $verify->check($code, $id);
    }
    public function getAllData($where='',$order=''){
        return $this->where($where)->order($order)->select();
    }
    public function getPageData($where='',$order='id desc',$perPage=20){
        $count = $this->where($where)->count();
        $Page = new \Think\Page($count,$perPage);//$Page=new \Org\Nx\Page($count,$perpage);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->where($where)->order($order)->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }
    public function addData($data){
        if($this->create($data)){
            if($this->add()){
                return array(
                    'status' => 'success',
                    'info' => '添加成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '添加失败！',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }
    }
    public function editData($data){
        if($this->create($data)){
            if(false!==$this->save()){
                //echo $this->_sql();die();
                return array(
                    'status' => 'success',
                    'info' => '修改成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '修改失败！',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }
    }
    public function getTree($where='',$order=''){
        $data=$this->where($where)->order($order)->select();
        return self::toLevel($data);
    }
    public function getMenu(){
        $where['is_menu']=1;
        $data=$this->where($where)->order("sort asc")->select();
        //return self::toLayer($data);
        $menu_data=self::toLayer($data);
        // 显示有权限的菜单
        $auth=new \Think\Auth();
        foreach ($menu_data as $k => $v) {
            if ($auth->check($v['name'],session('id'))) {
                foreach ($v['child'] as $m => $n) {
                    if(!$auth->check($n['name'],session('id'))){
                        unset($menu_data[$k]['child'][$m]);
                    }
                }
            }else{
                // 删除无权限的菜单
                unset($menu_data[$k]);
            }
        }
        return $menu_data;
    }
    //组成排序好的一维数组
    public function toLevel($data, $delimiter = '———', $parent_id = 0, $level = 0) {
        $arr = array();
        foreach ($data as $v) {
            if ($v['pid'] == $parent_id) {
                $v['level'] = $level + 1;
                $v['delimiter'] = str_repeat($delimiter, $level);
                $arr[] = $v;
                $arr = array_merge($arr, self::toLevel($data, $delimiter, $v['id'], $v['level']));
            }
        }
        return $arr;
    }
    //组成排序好的多维数组 多层次用于admin左右导航
    public function toLayer($data, $name = 'child', $parent_id = 0){
        $arr = array();
        foreach ($data as $v) {
            if ($v['pid'] == $parent_id) {
                $v[$name] = self::toLayer($data, $name, $v['id']);
                $arr[] = $v;
            }
        }
        return $arr;
    }


}