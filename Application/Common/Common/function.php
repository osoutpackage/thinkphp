<?php

//传递数据以易于阅读的样式格式化后输出
function p($data){
// 定义样式
$str='<pre style="display: block;padding: 9.5px;margin: 44px 0 0 0;font-size: 13px;line-height: 1.42857;color: #333;word-break: break-all;word-wrap: break-word;background-color: #F5F5F5;border: 1px solid #CCC;border-radius: 4px;">';
    // 如果是boolean或者null直接显示文字；否则print
    if (is_bool($data)) {
        $show_data=$data ? 'true' : 'false';
    }elseif (is_null($data)) {
        $show_data='null';
    }else{
        $show_data=print_r($data,true);
    }
    $str.=$show_data;
    $str.='</pre>';
echo $str;
}
/*
 * 上传图片
 * @param  string $file 表单上传按钮的name
 */
function uploadPic($file){
    if ($_FILES[$file]['tmp_name'] != "") {//获取上传图片
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize = 3145728;// 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath = './'; // 设置附件上传根目录
        $upload->savePath = '/Public/Uploads/pic/'; // 设置附件上传（子）目录
        $info = $upload->uploadOne($_FILES[$file]); // 上传单个文件
        if (!$info) {
            $data['error']=$upload->getError();
        } else {
            $data['file'] = $info['savepath'] . $info['savename'];
        }
    }else{
        $data['error']='没有找到上传的文件';
    }
    return $data;
}
/*
 * 上传文件
 */
function uploadFile($fileName){
    $upload = new \Think\Upload();// 实例化上传类
    $upload->maxSize = 3145728;// 设置附件上传大小3M
    $upload->exts = array('xls', 'xlsx');// 设置附件上传类型
    $upload->rootPath = './'; // 设置附件上传根目录
    $upload->savePath = './Public/Uploads/file/'; // 设置附件上传（子）目录 在Goods目录的上层，所以前面.
    $info = $upload->uploadOne($_FILES[$fileName]); // 上传单个文件
    if (!$info) {
        $data['error']=$upload->getError();
    } else {
        $data['file'] = $info['savepath'] . $info['savename'];
    }
    return $data;
}
/*
 * 淘宝Api接口
 */
function getItem($num_iid){
    //include THINK_PATH.'Library/Vendor/TaobaoApi/TopSdk.php';
    Vendor('TaobaoApi.TopSdk');
    date_default_timezone_set('Asia/Shanghai');
    $c = new \TopClient;
    $c->appkey = C('WEB_APPKEY');
    $c->secretKey = C('WEB_SECRETKEY');
    $req = new \TbkItemInfoGetRequest;
    $req->setFields("num_iid,title,pict_url,reserve_price,zk_final_price,user_type,provcity,item_url,nick,volume");
    $req->setPlatform("1");
    $req->setNumIids($num_iid);
    $resp = $c->execute($req);
    $items = $resp->results->n_tbk_item;
    $item_arr = (array)$items;//SimpleXMLElement Object转化为数组
    return $item_arr;
}
/**
 * 导入excel文件
 * @param  string $file excel文件路径
 * @return array        excel文件内容数组
 */
function importExcel($file){
    // 判断文件是什么格式
    $type = pathinfo($file);
    $type = strtolower($type["extension"]);
    if ($type=='xlsx') {
        $type='Excel2007';
    }elseif($type=='xls') {
        $type = 'Excel5';
    }
    //ini_set('max_execution_time', '0');//php执行时间 数值 0 表示没有执行时间的限制
    Vendor('PHPExcel.PHPExcel');//快速导入第三方框架类库
    // 判断使用哪种格式
    $objReader = PHPExcel_IOFactory::createReader($type);
    $objPHPExcel = $objReader->load($file);
    $sheet = $objPHPExcel->getSheet(0);
    // 取得总行数
    $highestRow = $sheet->getHighestRow();
    // 取得总列数
    $highestColumn = $sheet->getHighestColumn();
    //总列数转换成数字
    $numHighestColum = PHPExcel_Cell::columnIndexFromString("$highestColumn");
    //循环读取excel文件,读取一条,插入一条
    $data=array();
    //从第二行开始读取数据 根据表格决定
    for($j=2;$j<=$highestRow;$j++){
        //从A列读取数据
        for($k=0;$k<=$numHighestColum;$k++){
            //数字列转换成字母
            $columnIndex = PHPExcel_Cell::stringFromColumnIndex($k);
            // 读取单元格
            $data[$j][]=$objPHPExcel->getActiveSheet()->getCell("$columnIndex$j")->getValue();
        }
    }
    return $data;
}
/**
 * 发送邮件
 * @param  string $address 需要发送的邮箱地址 发送给多个地址需要写成数组形式
 * @param  string $subject 标题
 * @param  string $content 内容
 * @return boolean       是否成功
 */
function sendEmail($address,$subject,$content){
    $email_smtp=C('EMAIL_SMTP');
    $email_username=C('EMAIL_USERNAME');
    $email_password=C('EMAIL_PASSWORD');
    $email_from_name=C('EMAIL_FROM_NAME');
    $email_smtp_secure=C('EMAIL_SMTP_SECURE');
    $email_port=C('EMAIL_PORT');
    if(empty($email_smtp) || empty($email_username) || empty($email_password) || empty($email_from_name)){
        return array("status" => "error","message"=>'邮箱配置不完整');
    }
    require_once './ThinkPHP/Library/Org/Nx/class.phpmailer.php';
    require_once './ThinkPHP/Library/Org/Nx/class.smtp.php';
    $phpmailer=new \Phpmailer();
    // 设置PHPMailer使用SMTP服务器发送Email
    $phpmailer->IsSMTP();
    // 设置设置smtp_secure
    $phpmailer->SMTPSecure=$email_smtp_secure;
    // 设置port
    $phpmailer->Port=$email_port;
    // 设置为html格式
    $phpmailer->IsHTML(true);
    // 设置邮件的字符编码'
    $phpmailer->CharSet='UTF-8';
    // 设置SMTP服务器。
    $phpmailer->Host=$email_smtp;
    // 设置为"需要验证"
    $phpmailer->SMTPAuth=true;
    // 设置用户名
    $phpmailer->Username=$email_username;
    // 设置密码
    $phpmailer->Password=$email_password;
    // 设置邮件头的From字段。
    $phpmailer->From=$email_username;
    // 设置发件人名字
    $phpmailer->FromName=$email_from_name;
    // 添加收件人地址，可以多次使用来添加多个收件人
    if(is_array($address)){
        foreach($address as $addressv){
            $phpmailer->AddAddress($addressv);
        }
    }else{
        $phpmailer->AddAddress($address);
    }
    // 设置邮件标题
    $phpmailer->Subject=$subject;
    // 设置邮件正文
    $phpmailer->Body=$content;
    // 发送邮件。
    if(!$phpmailer->Send()) {
        $phpmailererror=$phpmailer->ErrorInfo;
        return array("status" => "error","message"=>$phpmailererror);
    }else{
        return array("status" => "success","message"=>'邮件发送成功');
    }
}
