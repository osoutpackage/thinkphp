<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;


class WebconfController extends AdminBaseController
{
    public function index(){
        $conf=D('Webconf');
        $conf_res=$conf->find(1);
        $this->assign('data',$conf_res);
        if(IS_POST){
            $data=$conf->saveConf();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $this->display();
    }

}