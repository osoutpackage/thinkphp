<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class MsgModel extends BaseModel  {
    protected $_validate = array(
        array('content','require','回复内容不能为空！',0,'regex',1),
    );
    protected $_auto = array (
        array('add_time','time',1,'function'),
    );
    /*
     * 管理员回复站内消息
     */
    public function replyData(){
        $data=I('post.');
        $data['fuid']=0;
        $data['fname']='SYSTEM';
        $result=$this->addData($data);
        return array(
            'status' => $result['status'],
            'info' => $result['info'],
        );
        /*
        if($this->create()){
            if($this->add()){
                return array(
                    'status' => 'success',
                    'info' => '发送成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '发送失败！',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }*/
    }
    /*
     * 管理员发送站内消息
     */
    public function addMsg(){
        if(I('tname')){
            //根据名字找出发送会员的id
            $User=M('User');
            $condition['username']=I('tname');
            $User_id=$User->where($condition)->getField('id');
            //echo $User->_sql();die();
            $data['tuid']=$User_id;
            $data['tname']=I('tname');
        }else{
            //名字留空向全站会员发送消息
            $data['tuid']=-1;
            $data['tname']='ALLUSER';
        }
        $data['content']=I('content');
        $data['fuid']=0;
        $data['fname']='SYSTEM';
        if($this->create($data)){
            if($this->add()){
                return array(
                    'status' => 'success',
                    'info' => '发送成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '发送失败！',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }
    }


}