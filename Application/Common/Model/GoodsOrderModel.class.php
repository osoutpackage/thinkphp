<?php
namespace Common\Model;
use Common\Model\BaseModel;

class GoodsOrderModel extends BaseModel  {

    protected $_auto = array (
        array('addtime','time',1,'function'),
    );

    public function addOrder($order_uid){

        $data = array('uid'=>$order_uid,'gid'=>I('id'),'number'=>1,'pay'=>10,'status'=>1);
        if ($this->addData($data)){
            if(D('Goods')->where('id='.I('id'))->setDec('number',1)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}