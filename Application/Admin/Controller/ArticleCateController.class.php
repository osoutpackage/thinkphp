<?php
/**
 *继承BaseController的方法
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class ArticleCateController extends AdminBaseController  {
    protected $_model='ArticleCate';
    protected $_order='sort asc';

}