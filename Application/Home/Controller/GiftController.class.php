<?php
/**
 * Created by PhpStorm.
 * User: lingda
 * Date: 2017/9/28 19:00
 */

namespace Home\Controller;
use Think\Controller;

class GiftController extends UserBaseController
{
    public function index(){
        $cate_list=M('ScoreItemCate')->select();
        //输出商品
        $item = D('ScoreItem');
        if(I('cid')){
            $where=array('cateid'=>I('cid'));
        }else{
            $where=null;
        }
        $data=$item->getPageData($where,$order='');
        $assign=array(
            'cate_list'=>$cate_list,
            'active_cid'=>I('cid'),
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function detail(){
        $cate_list=M('ScoreItemCate')->select();
        //输出商品详情
        $item=M('ScoreItem');
        $item_res = $item->find(I('id'));
        $item_res['info']=htmlspecialchars_decode($item_res['info']);
        $assign=array(
            'cate_list'=>$cate_list,
            'active_cid'=>I('cid'),
            'item'=>$item_res,
        );
        $this->assign($assign);
        $this->display();
    }
    public function order(){
        if(IS_POST){
            $order=D('ScoreOrder');
            if($order->create()){
                $order->uname=session('username');
                if($order->check_score()){
                    if($order->check_limit_num()){
                        if($order->deduct()&&$order->add()){
                            $this->success('兑换成功',U('User/gift'));
                        }else{
                            $this->error('兑换失败');
                        }
                    }else{
                        $this->error('兑换次数超限');
                    }
                }else{
                    $this->error('兑换失败，积分不足！');
                }
            }else{
                    $this->error($order->getError());
            }
            return;
        }else{
            $cate_list=M('ScoreItemCate')->select();
            $item=M('ScoreItem');
            $item_res = $item->find(I('id'));
            $assign=array(
                'cate_list'=>$cate_list,
                'item'=>$item_res,
            );
            $this->assign($assign);
        }
        $this->display();
    }


}