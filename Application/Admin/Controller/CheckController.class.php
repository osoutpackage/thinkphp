<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class CheckController extends AdminBaseController  {
    /*
     * 审核报名商品
     */
    protected $_model='Goods';
    public function index(){
        $goods = D('GoodsView'); // 实例化对象
        $data = $goods->getGoodsData($where='status = 0',$order='id desc');
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }

    public function pass(){
        $goods=M('goods');
        if($goods->where('id='.I('id'))->setField('status','2')){
            $this->ajaxReturn(array(
                'status' => 'success',
                'info' => '操作成功！',
            ));
        }else{
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '操作失败！',
            ));
        }
    }
    public function refuse(){
        $goods=M('goods');
        if($goods->where('id='.I('id'))->setField('status','1')){
            $this->ajaxReturn(array(
                'status' => 'success',
                'info' => '操作成功！',
            ));
        }else{
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '操作失败！',
            ));
        }
    }


}