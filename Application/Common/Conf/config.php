<?php
return array(
	//'配置项'=>'配置值'
    'LOAD_EXT_CONFIG' => 'webconf,db',//加载配置文件webconf.php和db.php //tags.php自动加载，无需另外引入
    //***********************************设置session**********************************//
    'SESSION_OPTIONS'         =>  array(
        'name'                =>  'lingda',                  //设置session名
        'prefix'              =>  'think_',                  // 设置前缀 避免跟HOME和ADMIN模块的冲突 HOME和ADMIN分别设置有不同前缀
        'expire'              =>  5*60*60,                   //SESSION保存时间
        'use_trans_sid'       =>  1,                         //跨页传递
        //'use_only_cookies'    =>  0,                       //是否只开启基于cookies的session的会话方式

    ),
    'COOKIE_EXPIRE' => 2*60*60, // Cookie有效期
    'COOKIE_DOMAIN' => '', // Cookie有效域名
    'COOKIE_PATH' => '/', // Cookie路径
    'COOKIE_PREFIX' => 'think_', // Cookie前缀 避免冲突
    'COOKIE_HTTPONLY' => '',
    //***********************************语言包功能**********************************//
    'LANG_SWITCH_ON' => true,//开启语言包功能
    'LANG_AUTO_DETECT' => true, // 自动侦测语言 开启多语言功能后有效
    'LANG_LIST' => 'zh-cn', // 允许切换的语言列表 用逗号分隔
    'VAR_LANGUAGE' => 'l', // 默认语言切换变量

    //***********************************邮件服务器**********************************//
    'EMAIL_FROM_NAME'        => 'linda',        // 发件人
    'EMAIL_SMTP'             => 'smtp.qq.com',  // smtp
    'EMAIL_USERNAME'         => '505761730@qq.com',        // 账号
    'EMAIL_PASSWORD'         => 'rbbqldwredxacaif',        // 密码  注意: 163和QQ邮箱是授权码；不是登录的密码bcnsdtlhjwjbbhdd
    'EMAIL_SMTP_SECURE'      => 'ssl',          // 如果使用QQ邮箱；需要把此项改为  ssl
    'EMAIL_PORT'             => '465',          // 如果使用QQ邮箱；需要把此项改为  465
	//***********************************定义常用路径**********************************//
	 'TMPL_PARSE_STRING'    => array( 
            '__PUBLIC__'        => __ROOT__ . '/Public',
            '__ADMIN_CSS__'     => __ROOT__ . trim(TMPL_PATH, '.') . 'Admin/Public/css',
            '__ADMIN_JS__'      => __ROOT__ . trim(TMPL_PATH, '.') . 'Admin/Public/js',
            '__ADMIN_IMAGES__'  => __ROOT__ . trim(TMPL_PATH, '.') . 'Admin/Public/images',
            '__ADMIN_PLUGINS__' => __ROOT__ . trim(TMPL_PATH, '.') . 'Admin/Public/plugins',
            '__PUBLIC_CSS__'    => __ROOT__ . trim(TMPL_PATH, '.') . 'Public/css',
            '__PUBLIC_JS__'     => __ROOT__ . trim(TMPL_PATH, '.') . 'Public/js',
            '__PUBLIC_IMAGES__' => __ROOT__ . trim(TMPL_PATH, '.') . 'Public/images',
        ),
	//***********************************跳转提示模板**********************************//
	'TMPL_ACTION_SUCCESS'  => 'Common:dispatch_jump',
    'TMPL_ACTION_ERROR'    => 'Common:dispatch_jump',
    //***********************************redis缓存配置**********************************//
    //'DATA_CACHE_KEY'=>'think',//文件缓存方式的安全机制
    'DATA_CACHE_PREFIX' => 'Redis_',//缓存前缀
    'DATA_CACHE_TYPE'=>'Redis',//默认动态缓存为Redis
    //'REDIS_RW_SEPARATE' => true, //Redis读写分离 true 开启
    'REDIS_HOST'=>'127.0.0.1', //redis服务器ip，多台用逗号隔开；读写分离开启时，第一台负责写，其它[随机]负责读；
    'REDIS_PORT'=>'6379',//端口
    'DATA_CACHE_TIME'=>'3600',//缓存时间
    //'DATA_CACHE_TIMEOUT'=>'300',//连接缓存服务器超时时间
    //'REDIS_PERSISTENT'=>false,//是否长连接 false=短连接
    //'REDIS_AUTH'=>'',//AUTH认证密码
    //***********************************子域名部署**********************************//


);