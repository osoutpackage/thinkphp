<?php
namespace Common\Model;
use Think\Model\ViewModel;
class ArticleViewModel extends ViewModel {
    public $viewFields = array(
        'Article'=>array('id','title','channel','sort','time','views','_type'=>'LEFT'),
        'ArticleCate'=>array('catename','_on'=>'Article.cateid=ArticleCate.id'),
        );
    public function getArticleData(){
        $count = $this->count();
        $Page = new \Think\Page($count,20);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->order('sort desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }


}