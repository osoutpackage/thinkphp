<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class SignModel extends BaseModel  {

        public function StartSign(){
            $where['uid'] = session('id');
            $signres = $this->where($where)->find();
            if($signres['last_date']){
                $today=date("Y-m-d",time());
                if($signres['last_date']==strtotime($today)){
                    return array(
                        'info' => '您今天已经签到过！',
                        'status' => 'error'
                    );
                }else{//执行签到操作
                    $data['id']=$signres['id'];
                    $data['last_date']=strtotime(date("Y-m-d",time()));
                    $data['sign_count']=$signres['sign_count']+1;
                    if($this->create($data)){
                        if($this->save()){
                            $data['uid']=$signres['uid'];
                            $data['username']=$signres['username'];
                            if($this->updateUserScore($data)) {
                                $this->addSignLog($data);//在return前插入签到的事件
                                return array(
                                    'info' => '签到成功！',
                                    'status' => 'success'
                                );
                            }else{ //无法更新用户积分
                                return array(
                                    'info' => '签到失败！',
                                    'status' => 'error'
                                );
                            }
                        }else{
                            return array(
                                'info' => '系统错误！',
                                'status' => 'error'
                            );
                        }
                    }else{
                        return array(
                            'info' => '系统错误！',
                            'status' => 'error'
                        );
                    }
                }
            }else{//没有签到记录，插入新人签到
                $data['uid']=session('id');
                $data['username']=session('username');
                $data['last_date']=strtotime(date("Y-m-d",time()));
                $data['sign_count']=1;
                if($this->create($data)){
                    if($this->add()){
                        if($this->updateUserScore($data)){
                            $this->addSignLog($data);//在return前插入签到的事件
                            //$this->success('sign success');
                            return array(
                                'info' => '签到成功！',
                                'status' => 'success'
                            );
                        }else{ //无法更新用户积分
                            return array(
                                'info' => '系统错误！',
                                'status' => 'error'
                            );
                        }
                    }else{
                        return array(
                            'info' => '系统错误！',
                            'status' => 'error'
                        );
                    }
                }else{
                    return array(
                        'info' => '系统错误！',
                        'status' => 'error'
                    );
                }
            }
        }
        /*
         * 更新用户积分
         */
        public function updateUserScore($data){
            $user = M('User');
            $condition['id']=$data['uid'];
            if($user->where($condition)->setInc('score',5)){ // 用户的积分加5
                return true;
            }else{
                return false;
            }
        }
        /*
         * 写入SignLog事件
         */
        public function addSignLog($data){
            $sign_log = D('SignLog');
            if ($sign_log->addSignLogAction($data)){
                return true;
            }else{
                return false;
            }

        }


}