<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class GoodsController extends AdminBaseController  {
    protected $_model='Goods';
    public function index(){
        $goods = D('GoodsView'); // 实例化对象
        $where['status'] = 2;//$where = array('status'=>2);
        $data = $goods->getGoodsData($where,$order='id desc');
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function add(){
        if(IS_POST){
            $goods=D('Goods');
            $data = $goods->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $cate=D('Cate');
        $cate_res=$cate->getTree($where='',$order='sort asc');
        $this->assign('cateres',$cate_res);
        $this->display();
    }
    public function edit(){
        $goods=D('Goods');
        if(IS_POST){
            $data = $goods->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $goods_res=$goods->find(I('id'));
            $cate=D('Cate');
            $cate_res=$cate->getTree($where='',$order='sort asc');
            $assign=array(
                'goods'=>$goods_res,
                'cateres'=>$cate_res,
            );
            $this->assign($assign);
        }
        $this->display();
    }
    /*
     * 批量删除商品
     */
    public function bdel(){
        $ids_arr=I('id');
        if($ids_arr){
            $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
            $goods=D('Goods');
            if($goods->delete($ids)){
                $this->success('批量删除成功',U('index'));
            }else{
                $this->error('批量删除失败');
            }
        }else{
            $this->error('未选择有数据');
        }
    }
    public function sort(){
        $goods=D('Goods');
        foreach ($_POST as $id=>$sort){
            $goods->where(array('id'=>$id))->setField('sort',$sort);
        }
        $this->success("更新排序成功");
        //var_dump($_POST);
    }
    public function getItemInfoByApi(){
        $num_iid = I('num_iid');
        if(!IS_AJAX){
            $this->error('非法访问');
            exit();
        }
        if(empty($num_iid)){
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '请输入正确的商品链接！',
            ));
        }
        $item_arr = getItem($num_iid);
        //p($item_arr);
        if($item_arr){
            $this->ajaxReturn(array(
                'status' => 'success',
                'info' => $item_arr,
            ));
        }
    }
    /*
     * 后台导入阿里妈妈高佣金采集的商品，必须是excel表格形式
     */
    public function import_goods(){
        if(IS_POST){
            $goods=D('Goods');
            $data = $goods->uploadGoods();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate=D('Cate');
            $cate_res=$cate->getTree($where='',$order='sort asc');
            $this->assign('cateres',$cate_res);
            $this->display();
        }
        //p($item);
    }




}