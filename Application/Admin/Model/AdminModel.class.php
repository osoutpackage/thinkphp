<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Model;
use Common\Model\BaseModel;

class AdminModel extends BaseModel  {
        protected $_validate = array(
        array('username','require','管理员不能为空！',1,'regex',3),
        array('username','','管理员不得重复！',1,'unique',3),
        array('password','require','密码不能为空！',1,'regex',1),
        array('username','require','管理员不能为空！',1,'regex',4),
        array('verify','check_verify','验证码不正确！',1,'callback',4),//，你可以给登录操作专门指定验证时间为4,只在登录时候验证
    );
    protected $_auto = array (
        array('password','md5',3,'function'),
        array('password','',2,'ignore'),//编辑的时候password留空的话则忽略
    );
    public function login(){
        $password=$this->password;//create后password已经自动完成md5了下面可以直接判断
        $info=$this->where(array('username'=>$this->username))->find();
        if($info){
            if($info['password']==$password){
                //$_SESSION['admin']=array(
                //    'id'=>$info['id'],
                //   'username'=>$info['username'],
                //);
                session('id',$info['id']);
                session('username',$info['username']);
                $this-> where('id='.$info['id'])->setField('login_time',time());
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function delAdminById(){
        if(I('id')==1){
            return array(
                'status' => 'error',
                'info' => '初始管理员不能删除！',
            );
        }else{
            if($this->delete(I('id'))){
                //从auth_group_access表中删除该管理员记录
                $role=D('AuthGroupAccess');
                $role->where('uid = '.I('id'))->delete();
                return array(
                    'status' => 'success',
                    'info' => '删除成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '删除失败！',
                );
            }
        }
    }


}