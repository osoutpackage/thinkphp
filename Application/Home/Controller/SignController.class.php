<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 * Date: 2017/10/1 23:19
 */

namespace Home\Controller;
use Think\Controller;

class SignController extends Controller
{
        public function sign(){
            if(session('id')==''){
                $this->ajaxReturn(array(
                    'info' => '请先登录！',
                    'status' => 'login',
                    'callback' => U('User/login')
                ));
            }
            if(!IS_AJAX){
                $this->ajaxReturn(array(
                    'info' => '非法访问方式！',
                    'status' => 'error'
                ));
                exit();
            }
            $sign = D('Sign');
            $result=$sign->StartSign();
            $this->ajaxReturn(array(
                'status' => $result['status'],
                'info' => $result['info'],
            ));
        }



}