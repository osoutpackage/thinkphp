<?php
namespace Home\Controller;
use Common\Controller\HomeBaseController;

class IndexController extends HomeBaseController  {
    public function index(){
        //header("Content-Type:text/html; charset=utf-8");
        //$parm=base64_encode("invite=5");//加密方法
        $url=$_SERVER['QUERY_STRING'];//获取url?以后的内容，判断是否邀请链接
        if($url){
            $str=base64_decode($url);//解密
            if(strpos($str,'invite=')===false){
                return;
            }else{
                //print_r($str);
                //正则表达式
                $regex='/invite\=([\d]+?)/';
                $wha=preg_match($regex, $str, $result);
                //dump($result);
                $parent_id=$result[1];
                session('parent_id',$parent_id);
            }
        }
        /*
         * 获取首页商品列表
         */
        if(I('cateid')){
            $where['cateid']=I('cateid');
        }
        $where['status'] = 2;
        $where['endtime'] = array('gt',time());
        $where['starttime'] = array('lt',time());
        $goods = D('Goods'); // 实例化Data数据对象
        $data = $goods->getGoodsData($where,$order='sort desc',$perPage=80,$cache=true);
        //echo I('p');
        //echo $goods->_sql();
        $assign=array(
            'list'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function search(){
        if(I('k')){
            $keywords=urldecode(I('k'));
            $where['title'] = array('like','%'.$keywords.'%');
        }
        $goods = D('Goods'); // 实例化Data数据对象
        $count = $goods->where($where)->count();
        $Page = new \Think\Page($count,60);
        foreach($where as $key=>$val) {
            $Page->parameter[$key] = urlencode($val);//分页跳转的时候保证查询条件
        }
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $goods->order('sort desc')->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
        $assign=array(
            'list'=>$list,
            'page'=>$show,
        );
        $this->assign($assign);
        $this->display('index');
    }
}