<?php
/**
 * 基类控制器，这里提供了基本的增删改查功能
 * 所有的模型都继承了BaseModel里的getData、addData、editData
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Controller;
use Think\Controller;

class BaseController extends Controller
{
    protected $_model;//这个控制器要使用的模型,在子控制器中重新定义模型
    protected $_perpage=20;//默认每页显示20条数据，根据情况在子控制器重新定义
    protected $_order='id desc'; //根据需要在子控制器定义排序方法
    /*
     * 定义一个_where方法根据需要进行条件查询，默认_where是返回空的，可以在子控制器中重新定义_where方法覆盖父类
     */
    protected function _where(){
        return [];
    }
    /* @$_order变量
     * @_where()方法
     */
    public function index(){
        $model=D($this->_model);
        $where=$this->_where();
        $order=$this->_order;
        $perpage=$this->_perpage;
        $data = $model->getPageData($where,$order,$perpage);
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function add(){
        $model=D($this->_model);
        if(IS_POST){
            $data = $model->addData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }
        $this->display();
    }

    public function edit(){
        $model=D($this->_model);
        if(IS_POST){
            $data = $model->editData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
            return;
        }else{
            $result=$model->find(I('id'));
            $this->assign('data',$result);
        }
        $this->display();
    }
    public function del(){
        $model=D($this->_model);
        if($model->delete(I('id'))){
            $this->ajaxReturn(array(
                'status' => 'success',
                'info' => '删除成功！',
            ));
        }else{
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '删除失败！',
            ));
        }
    }
    public function bDel(){
        $ids_arr=I('id');
        if($ids_arr){
            $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
            $model=D($this->_model);
            if($model->delete($ids)){
                $this->ajaxReturn(array(
                    'status' => 'success',
                    'info' => '批量删除成功！',
                ));
            }else{
                $this->ajaxReturn(array(
                    'status' => 'error',
                    'info' => '批量删除失败！',
                ));
            }
        }else{
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '未选择有数据！',
            ));
        }
    }

}