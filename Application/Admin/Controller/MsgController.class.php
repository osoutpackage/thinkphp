<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class MsgController extends AdminBaseController  {
    protected $_model='Msg';
    /*
     * 提供搜索提交给父类BaseController的index方法
     */
    protected function _where(){
        return 'tuid = 0';
    }

    public function reply(){
        $msg=D('Msg');
        if(IS_POST){
            $data = $msg->replyData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
            return;
        }else{
            $msg_content=$msg->find(I('id'));
            $this->assign('data',$msg_content);
        }
        $this->display();
    }
    public function read(){
        $msg=D('Msg');
        $msg_res=$msg->find(I('id'));
        if($msg_res['status']==0){
            $msg-> where('id='.I('id'))->setField('status',1);
        }
        $this->assign('data',$msg_res);
        $this->display();
    }
    /* 后置操作，但是由于$msg_res['status']值无法传递过来，此方法行不通
    public function _after_read(){
        $msg=M('Msg');
        if($msg_res['status']==0){
            $msg-> where('id='.I('id'))->setField('status',2);
        }
    }
    */
    public function add(){
        if(IS_POST) {
            $msg=D('Msg');
            $data = $msg->addMsg();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
            return;
        }
        $this->display();
    }
}