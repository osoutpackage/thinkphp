-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 �?10 �?31 �?14:22
-- 服务器版本: 5.5.53
-- PHP 版本: 5.5.38

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `thinkphp_mall`
--

-- --------------------------------------------------------

--
-- 表的结构 `think_admin`
--

DROP TABLE IF EXISTS `think_admin`;
CREATE TABLE IF NOT EXISTS `think_admin` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` char(32) NOT NULL,
  `login_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `think_admin`
--

INSERT INTO `think_admin` (`id`, `username`, `password`, `login_time`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1509433350),
(7, 'admin2', 'c3284d0f94606de1fd2af172aba15bf3', NULL),
(8, 'admin3', 'c3284d0f94606de1fd2af172aba15bf3', NULL),
(14, 'admin4', '21232f297a57a5a743894a0e4a801fc3', NULL),
(19, 'admin11', '698d51a19d8a121ce581499d7b701668', NULL),
(20, '122', '6512bd43d9caa6e02c990b0a82652dca', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `think_article`
--

DROP TABLE IF EXISTS `think_article`;
CREATE TABLE IF NOT EXISTS `think_article` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `channel` varchar(60) DEFAULT NULL,
  `sort` smallint(3) NOT NULL DEFAULT '0',
  `cateid` smallint(3) NOT NULL,
  `content` text,
  `time` int(12) NOT NULL,
  `views` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `think_article`
--

INSERT INTO `think_article` (`id`, `title`, `describe`, `channel`, `sort`, `cateid`, `content`, `time`, `views`) VALUES
(8, '关于我们的', '关注我们', 'admin', 0, 1, '&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 购优惠宗旨是方便网友们快速找到物美价廉的商品，而不必在淘宝网中到处搜索，我们不仅为您展示淘宝网上所有的物超所值的商品，同时也积极与淘宝卖家沟通合作，为网友们提供更多优质的折扣商品。 网站的优惠折扣商品信息，每天更新。\r\n　　本站所有的商品信息都来源于淘宝网，网友点击商品链接会跳转进入淘宝网，所有交易均在淘宝网进行，请放心使用本网站。\r\n　　网站自发布以来，一直坚持用户至上原则，您的满意是我们前进的动力。在此感谢一直以来支持本站的朋友。 &amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;/p&gt;', 1506574472, 0),
(9, '商家合作', '', 'admin', 0, 15, '&lt;h3 class=&quot;mb10&quot; style=&quot;margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; font-size: 16px; color: rgb(51, 51, 51); font-weight: normal; font-family: &amp;#39;Microsoft Yahei&amp;#39;, Tahoma, Helvetica, Arial, sans-serif, 宋体; white-space: normal; margin-bottom: 10px !important; background-color: rgb(255, 255, 255);&quot;&gt;商家合作&lt;/h3&gt;&lt;h2 style=&quot;margin: 0px; padding: 10px 0px; font-size: 14px; color: rgb(94, 94, 94); white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;一、招商报名规则：&lt;/h2&gt;&lt;table width=&quot;700&quot; class=&quot;ke-zeroborder&quot;&gt;&lt;tbody style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot; class=&quot;firstRow&quot;&gt;&lt;th style=&quot;margin: 0px; padding: 0px;&quot;&gt;活动价格&lt;/th&gt;&lt;th style=&quot;margin: 0px; padding: 0px;&quot;&gt;9.9包邮&lt;/th&gt;&lt;th style=&quot;margin: 0px; padding: 0px;&quot;&gt;10-20元包邮&lt;/th&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;活动价格&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;9.9包邮&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;10-20元包邮&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;C店要求&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;1红心以上&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;1钻以上&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;B店要求&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;近30日销量&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;动态评分&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;同行持平及以上&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;同行持平及以上&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;商品评价&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;不限&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;报名数量&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;&amp;gt;200件&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;&amp;gt;200件&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;历史价格&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;&amp;gt;本活动价格&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;&amp;gt;本活动价格&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;审核周期&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;3工作日&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;3工作日&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;打折方式&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;限时、VIP&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;限时、VIP&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;准时开始&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;定时上架&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;定时上架&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;重复周期&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;一件商品只上一次&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;一件商品只上一次&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;店内客服&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;必须在线&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;必须在线&lt;/td&gt;&lt;/tr&gt;&lt;tr style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;优先通过&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;价值较大商品&lt;/td&gt;&lt;td style=&quot;margin: 0px; padding: 0px; text-indent: 12px;&quot;&gt;价值较大商品&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;h2 style=&quot;margin: 0px; padding: 10px 0px; font-size: 14px; color: rgb(94, 94, 94); white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;二、活动流程&lt;/h2&gt;&lt;p class=&quot;sphz_p&quot; style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;1、商家认真查看活动要求说明进行报名。&lt;br/&gt;2、准确填写报名信息，提交报名，我们会在三天内进行审核，请关注自己的报名后台查看审核结果。。&lt;br/&gt;3、审核通过的活动，我们会联系商家，沟通活动准备工作及上线排期等问题。&lt;/p&gt;&lt;h2 style=&quot;margin: 0px; padding: 10px 0px; font-size: 14px; color: rgb(94, 94, 94); white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;三、黑名单&lt;/h2&gt;&lt;p class=&quot;sphz_p&quot; style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;1、拉黑原因（重要）&amp;nbsp;&lt;br/&gt;如果商家有以下行为，我们将终止与该商家合作，并将该商家列入黑名单永不合作。 我们真诚的提醒您，诚信合作，拉黑对您对我们都是损失！&amp;nbsp;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;1.1、绕圈报名上活动，同款商品刻意联系不同专员审核者，发现即拉黑，活动随时终止；&amp;nbsp;&lt;br/&gt;1.2、未按约定排期上架宝贝，修改宝贝价格、宝贝标题，活动时间以及添加我们的宣传图片；&amp;nbsp;&lt;br/&gt;1.3、活动中单方面将宝贝下架；&amp;nbsp;&lt;br/&gt;1.4、活动中单方面修改价格或包邮情况；&amp;nbsp;&lt;br/&gt;1.5、活动中删除宝贝标题中的【独享】，或是删除我们的宣传图片或链接；&amp;nbsp;&lt;br/&gt;1.6、活动期间同时参加其他促销活动；&amp;nbsp;&lt;br/&gt;1.7、售卖假货或劣质产品等欺骗消费者的行为；&amp;nbsp;&lt;br/&gt;1.8、未履行相应的售后服务，未能及时恰当处理用户投诉问题；&amp;nbsp;&lt;br/&gt;1.9、活动期间悬挂过多其他活动宣传图片，恶意引导用户，造成用户误解的行为；&amp;nbsp;&lt;br/&gt;1.10、其他不诚信以及违规行为，最终解释权归本活动所有。&amp;nbsp;&lt;/span&gt;2、其他说明&amp;nbsp;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;2.1、加入黑名单的商家，本活动永不合作，且无法删除此黑名单。&amp;nbsp;&lt;br/&gt;2.2、黑名单由招商专员或活动排查专员添加。&lt;/span&gt;&lt;/p&gt;&lt;h2 style=&quot;margin: 0px; padding: 10px 0px; font-size: 14px; color: rgb(94, 94, 94); white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;四、联系方式&lt;/h2&gt;&lt;p class=&quot;sphz_p&quot; style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;sphz_span&quot; style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;商务合作QQ：&lt;/strong&gt;928242318&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', 1506574534, 0),
(10, '联系我们', '', 'admin', 0, 3, '&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　如果您对我们的网站以及服务有任何疑问、意见或建议，请随时与我们在线客服取得联系！我们的客服人员会给您详尽细致的解答，您的意见或建议将立即得到反馈。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　工作时间：周一至周五（09：00 - 12：00　14：00 - 18：00）&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', 1506575182, 0),
(11, '免责声明', '', 'admin', 0, 4, '&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;尊敬的用户：&lt;/strong&gt;&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　您即将通过本页面确认使用购优惠以外的第三方卖家为您提供的商品和相关服务需注意的相关事项。在接受本须知内容之前，请您仔细阅读本须知的 全部内容如果您对本须知的条款有疑问的，请通过&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;客服渠道进行询问，&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;将向您解释条款内容。如果您不同意本须知的任意内容，或者无法准确理解&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;对条款的解释，请不要进行后续操作。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;使用须知（简称“本须知”）是&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;用户(以下简称“用户”或“您”)就使用基于在&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;的卖家（以下简称“卖家”）提供相关商品或服务所签署的有效文件。用户通过网络页面点击确认或以其他方式选择接受本须知，即表示您同意接受以下全部内容：&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　1．在您在本网站中与卖家产生的所有交易，均在淘宝网内进行，对于交易过程中的资金及个人信息的安全问题，均由淘宝网负责，本网站不承担任何交易过程产生的资金及个人信息安全问题的责任。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　2．卖家提供的产品和服务由卖家独立负责。在交易过程中遇到任何质量或者服务问题，均可以按照正常的淘宝维权流程进行维权，本网站不对卖家提供的商品或服务承担任何责任。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　3．为了保障您的权益，本网站将定期对卖家提供的商品宣传进行检查，但您理解检查不可能每时每刻都在进行。所以，请您仔细阅读卖家对商品的各种宣传资料，对其做出准确判断。本网站对卖家所涉及的商品和服务任何内容、宣传、和其他材料未作认证，也不对此负责或承担任何责任。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　4．网络欺诈往往会模仿、仿冒淘宝网登录页面的样式制作视觉感相似的页面诱导您输入账号和密码等信息，&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;提醒您防范网络风险，不要向任何人士（包括卖家）透露淘宝网账号、密码等相关信息。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　5．您理解并同意， &lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;网仅提供淘宝网卖家的商品链接，对于卖家的商品服务的选择和交易的内容，应由您自行判断，且审慎交易。您需自行了解卖家提供的商品或服务的功能、收费标准、退款规则、服务有效期等情况并注意交易风险。秒杀网不对卖家和用户行为的合法性、有效性及卖家提供商品和服务的真实性、合法性和有效性作任何明示和默示的担保。&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;网用户与卖家产生纠纷时将努力进行协调，卖家也应秉承为用户提供优质商品和服务的理念为用户提供便利，但&lt;span style=&quot;color: rgb(94, 94, 94); font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;购优惠&lt;/span&gt;网并不保证协调取得实际效果。&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(94, 94, 94); font-size: 12px; white-space: normal; font-family: Tahoma, Helvetica, Arial, 宋体, sans-serif; line-height: 30px; background-color: rgb(255, 255, 255);&quot;&gt;　　6．如您发现卖家侵犯您的合法权益的，或您不幸遭遇网络欺诈的，请及时联系淘宝网，本网站会协助您维护您的合法权益。&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', 1506575301, 0);

-- --------------------------------------------------------

--
-- 表的结构 `think_article_cate`
--

DROP TABLE IF EXISTS `think_article_cate`;
CREATE TABLE IF NOT EXISTS `think_article_cate` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `catename` varchar(60) NOT NULL,
  `sort` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `think_article_cate`
--

INSERT INTO `think_article_cate` (`id`, `catename`, `sort`) VALUES
(1, '关于我们', 1),
(13, '帮助中心', 2),
(3, '联系我们', 3),
(4, '免责声明', 5),
(6, '常见问题', 6),
(15, '商家合作', 4);

-- --------------------------------------------------------

--
-- 表的结构 `think_auth_group`
--

DROP TABLE IF EXISTS `think_auth_group`;
CREATE TABLE IF NOT EXISTS `think_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text COMMENT '规则id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户组表' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `think_auth_group`
--

INSERT INTO `think_auth_group` (`id`, `title`, `status`, `rules`) VALUES
(1, '超级管理员', 1, '3,10,12,13,11,72,46,47,48,49,73,74,75,76,77,78,79,80,14,15,16,17,18,4,6,8,9,7,41,43,44,45,29,30,31,32,33,34,35,36,37,38,39,40,50,51,59,60,61,62,55,63,64,65,66,52,67,68,69,70,71,53,25,26,27,28,19,20,22,23,21,54,56,57,58,24,1'),
(5, '商品组', 1, '3,10,12,13,11,46,47,48,49,53,24,1'),
(4, '文章组', 1, NULL),
(6, '会员组', 1, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `think_auth_group_access`
--

DROP TABLE IF EXISTS `think_auth_group_access`;
CREATE TABLE IF NOT EXISTS `think_auth_group_access` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

--
-- 转存表中的数据 `think_auth_group_access`
--

INSERT INTO `think_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1),
(7, 4),
(8, 5),
(14, 6),
(19, 4),
(20, 5);

-- --------------------------------------------------------

--
-- 表的结构 `think_auth_rule`
--

DROP TABLE IF EXISTS `think_auth_rule`;
CREATE TABLE IF NOT EXISTS `think_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0不是菜单，1是菜单',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `sort` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='规则表' AUTO_INCREMENT=81 ;

--
-- 转存表中的数据 `think_auth_rule`
--

INSERT INTO `think_auth_rule` (`id`, `pid`, `name`, `title`, `status`, `type`, `is_menu`, `condition`, `sort`) VALUES
(1, 54, 'Index/index', '后台首页', 1, 1, 0, '', 5),
(3, 0, 'Goods/', '商品管理', 1, 1, 1, '', 1),
(4, 0, 'Article/', '文章管理', 1, 1, 1, '', 4),
(6, 4, 'Article/index', '文章列表', 1, 1, 1, '', 1),
(7, 4, 'Article/add', '发布文章', 1, 1, 1, '', 2),
(8, 6, 'Article/edit', '编辑文章', 1, 1, 0, '', 3),
(9, 6, 'Article/del', '删除文章', 1, 1, 0, '', 4),
(10, 3, 'Goods/index', '商品列表', 1, 1, 1, '', 1),
(11, 3, 'Goods/add', '添加商品', 1, 1, 1, '', 2),
(12, 10, 'Goods/edit', '编辑商品', 1, 1, 0, '', 3),
(13, 10, 'Goods/del', '删除商品', 1, 1, 0, '', 4),
(14, 0, 'Cate/', '商品分类', 1, 1, 1, '', 3),
(15, 14, 'Cate/index', '分类列表', 1, 1, 1, '', 1),
(16, 14, 'Cate/add', '添加分类', 1, 1, 1, '', 2),
(17, 14, 'Cate/edit', '编辑分类', 1, 1, 0, '', 2),
(18, 14, 'Cate/del', '删除分类', 1, 1, 0, '', 4),
(19, 0, 'Link', '友情链接', 1, 1, 1, '', 9),
(20, 19, 'Link/index', '友链列表', 1, 1, 1, '', 1),
(21, 19, 'Link/add', '添加友链', 1, 1, 1, '', 2),
(22, 20, 'Link/edit', '修改友链', 1, 1, 0, '', 3),
(23, 20, 'Link/del', '删除友链', 1, 1, 0, '', 4),
(24, 54, 'Index/main', '欢迎页面', 1, 1, 0, '', 4),
(25, 0, 'User/', '会员管理', 1, 1, 1, '', 8),
(26, 25, 'User/index', '会员列表', 1, 1, 1, '', 1),
(27, 26, 'User/del', '删除会员', 1, 1, 0, '', 2),
(28, 25, 'SignLog/index', '签到列表', 1, 1, 1, '', 3),
(29, 0, 'ScoreItem/', '积分商城', 1, 1, 1, '', 6),
(30, 29, 'ScoreItem/index', '兑换商品列表', 1, 1, 1, '', 1),
(31, 30, 'ScoreItem/add', '添加兑换商品', 1, 1, 0, '', 2),
(32, 30, 'ScoreItem/edit', '编辑兑换商品', 1, 1, 0, '', 3),
(33, 30, 'ScoreItem/del', '删除兑换商品', 1, 1, 0, '', 4),
(34, 29, 'ScoreItemCate/index', '兑换分类列表', 1, 1, 1, '', 5),
(35, 34, 'ScoreItemCate/add', '添加兑换分类', 1, 1, 0, '', 6),
(36, 34, 'ScoreItemCate/edit', '编辑兑换分类', 1, 1, 0, '', 7),
(37, 34, 'ScoreItemCate/del', '删除兑换分类', 1, 1, 0, '', 8),
(38, 29, 'ScoreOrder/index', '兑换订单列表', 1, 1, 1, '', 9),
(39, 38, 'ScoreOrder/edit', '兑换订单发货', 1, 1, 0, '', 10),
(40, 38, 'ScoreOrder/del', '删除兑换订单', 1, 1, 0, '', 11),
(41, 4, 'ArticleCate/index', '文章栏目列表', 1, 1, 1, '', 5),
(55, 50, 'AuthGroup/index', '角色管理', 1, 1, 1, '', 2),
(43, 41, 'ArticleCate/add', '添加文章栏目', 1, 1, 0, '', 2),
(44, 41, 'ArticleCate/edit', '编辑文章栏目', 1, 1, 0, '', 3),
(45, 41, 'ArticleCate/del', '删除文章栏目', 1, 1, 0, '', 4),
(46, 3, 'Check/index', '审核商品', 1, 1, 1, '', 5),
(47, 46, 'Check/pass', '审核通过', 1, 1, 0, '', 1),
(48, 46, 'Check/refuse', '拒绝商品', 1, 1, 0, '', 2),
(49, 46, 'Check/del', '删除商品', 1, 1, 0, '', 3),
(50, 0, 'AuthRule/', '权限管理', 1, 1, 1, '', 7),
(51, 50, 'AuthRule/index', '权限列表', 1, 1, 1, '', 1),
(52, 50, 'Admin/index', '管理员列表', 1, 1, 1, '', 3),
(53, 52, 'Admin/logout', '退出登录', 1, 1, 0, '', 5),
(54, 0, 'Webconf/', '系统管理', 1, 1, 1, '', 11),
(56, 54, 'Msg/index', '站内信', 1, 1, 1, '', 1),
(57, 54, 'Webconf/index', '系统设置', 1, 1, 1, '', 2),
(58, 54, 'Index/delcache', '清除缓存', 1, 1, 1, '', 3),
(59, 51, 'AuthRule/add', '添加权限', 1, 1, 0, '', 1),
(60, 51, 'AuthRule/edit', '修改权限', 1, 1, 0, '', 2),
(61, 51, 'AuthRule/addchild', '添加子权限', 1, 1, 0, '', 3),
(62, 51, 'AuthRule/del', '删除权限', 1, 1, 0, '', 4),
(63, 55, 'AuthGroup/add', '添加角色', 1, 1, 0, '', 1),
(64, 55, 'AuthGroup/edit', '编辑角色', 1, 1, 0, '', 2),
(65, 55, 'AuthGroup/del', '删除角色', 1, 1, 0, '', 3),
(66, 55, 'AuthGroup/giverule', '分配权限', 1, 1, 0, '', 5),
(67, 52, 'Admin/add', '添加管理员', 1, 1, 0, '', 1),
(68, 52, 'Admin/edit', '编辑管理员', 1, 1, 1, '', 2),
(69, 52, 'Admin/del', '删除管理员', 1, 1, 0, '', 3),
(70, 52, 'Admin/add_role', '分配角色', 1, 1, 0, '', 4),
(71, 52, 'Admin/edit_role', '修改角色', 1, 1, 0, '', 5),
(72, 11, 'Goods/getItemInfoByApi', '通过API获取商品信息', 1, 1, 0, '', 5),
(73, 3, 'Goods/import_goods', '导入商品', 1, 1, 1, '', 6),
(74, 0, 'Mall/', '自营商城', 1, 1, 1, '', 2),
(75, 74, 'Mall/index', '商品列表', 1, 1, 1, '', 1),
(76, 75, 'Mall/add', '发布商品', 1, 1, 0, '', 1),
(77, 75, 'Mall/edit', '修改商品', 1, 1, 0, '', 2),
(78, 75, 'Mall/del', '删除商品', 1, 1, 0, '', 3),
(79, 75, 'Mall/bDel', '批量删除', 1, 1, 0, '', 6),
(80, 74, 'GoodsOrder/index', '订单列表', 1, 1, 1, '', 2);

-- --------------------------------------------------------

--
-- 表的结构 `think_cate`
--

DROP TABLE IF EXISTS `think_cate`;
CREATE TABLE IF NOT EXISTS `think_cate` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `catename` varchar(60) NOT NULL,
  `pid` smallint(6) NOT NULL DEFAULT '0',
  `addtime` int(12) NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `sort` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- 转存表中的数据 `think_cate`
--

INSERT INTO `think_cate` (`id`, `catename`, `pid`, `addtime`, `is_show`, `sort`) VALUES
(1, '女装', 0, 0, 1, 1),
(2, '男装', 0, 0, 1, 2),
(35, '童鞋', 33, 1506230578, 1, 0),
(9, '女裙子', 1, 1506154342, 1, 3),
(7, '女衬衫', 1, 1506154226, 1, 1),
(8, '男裤', 2, 1506154276, 1, 0),
(10, '长裙', 9, 1506161644, 1, 0),
(11, '长裤', 8, 1506161745, 1, 0),
(12, '短裤', 8, 1506161960, 1, 0),
(37, '短裙', 9, 1507434077, 1, 2),
(34, '童装', 33, 1506230568, 1, 0),
(33, '儿童', 0, 1506230558, 1, 2),
(38, '母婴', 0, 1507434109, 1, 6),
(39, '美食', 0, 1507435191, 1, 7),
(40, '鞋包', 0, 1507435275, 1, 8),
(41, '奶粉', 38, 1507888494, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `think_category`
--

DROP TABLE IF EXISTS `think_category`;
CREATE TABLE IF NOT EXISTS `think_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `model_id` int(10) NOT NULL COMMENT '模型id',
  `model_flag` varchar(20) NOT NULL COMMENT '模型标示符',
  `title` varchar(45) NOT NULL COMMENT '分类名称',
  `flag` varchar(255) NOT NULL COMMENT '栏目标示符，必须为字母或数字',
  `content` mediumtext NOT NULL COMMENT '内容，仅用于单页内容',
  `has_cover` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启封面',
  `cover` varchar(255) NOT NULL COMMENT '分类封面图',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `sort` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示，1：显示，0：不显示，默认1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flag` (`flag`) COMMENT '标示符'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='栏目表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `think_goods`
--

DROP TABLE IF EXISTS `think_goods`;
CREATE TABLE IF NOT EXISTS `think_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `cateid` smallint(3) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `promotion_price` decimal(10,2) NOT NULL,
  `discount` decimal(10,1) NOT NULL,
  `site` enum('taobao','tmall','mall') NOT NULL,
  `num_iid` bigint(11) unsigned DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `tk_url` varchar(255) DEFAULT NULL,
  `coupons` varchar(255) DEFAULT NULL,
  `coupons_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pic` varchar(255) NOT NULL,
  `addtime` int(10) NOT NULL,
  `starttime` int(10) NOT NULL,
  `endtime` int(10) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `number` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品库存，非负数',
  `seller_id` int(10) NOT NULL DEFAULT '0',
  `sort` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '2',
  `recom` tinyint(1) NOT NULL DEFAULT '0',
  `ispost` tinyint(1) NOT NULL DEFAULT '0',
  `collect` mediumint(8) NOT NULL DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `num_iid` (`num_iid`),
  KEY `starttime` (`starttime`),
  KEY `endtime` (`endtime`),
  KEY `status` (`status`),
  KEY `cateid` (`cateid`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `think_goods`
--

INSERT INTO `think_goods` (`id`, `title`, `cateid`, `price`, `promotion_price`, `discount`, `site`, `num_iid`, `url`, `tk_url`, `coupons`, `coupons_price`, `pic`, `addtime`, `starttime`, `endtime`, `nick`, `number`, `seller_id`, `sort`, `status`, `recom`, `ispost`, `collect`, `remark`) VALUES
(8, '春季连衣裙韩版中长款加厚针织打底裙子长袖修身显瘦秋装女冬裙潮', 1, '129.00', '39.90', '3.1', 'taobao', 541186667449, 'https://detail.tmall.com/item.htm?id=541186667449', 'https://s.click.taobao.com/t?e=m%3D2%26s%3Dama4kXUrHYscQipKwQzePOeEDrYVVa64K7Vc7tFgwiHjf2vlNIV67s50cfaq%2B6NE7LeyVIpD4ejLrAnjPuQ11wlgr6JkJaDvrIM6GqaIxk8H6rDOrgNgkF%2BjZuXo5GcewC5wD8mGS5pB7m5k0uwB1vs3lM5%2FF8Coxg5p7bh%2BFbQ%3D&amp;pvid=12_125.118.63.39_100', '', '0.00', '/Public/Uploads/pic/2017-09-21/59c3d6c5de891.jpg', 1506006725, 1505923200, 1506528000, '西奴狐旗舰店', 0, 0, 1, 0, 1, 1, 0, '商品备注'),
(17, '秋季青少年休闲衬衫男长袖潮流修身帅气上衣春秋打底衬衣男装显瘦', 2, '199.00', '29.90', '1.5', 'tmall', 557543345934, 'https://detail.tmall.com/item.htm?id=557543345934', 'https://s.click.taobao.com/t?e=m%3D2%26s%3D5XthVpbnIrkcQipKwQzePOeEDrYVVa64K7Vc7tFgwiHjf2vlNIV67jXobfVPd3TG0e71iVTN2RzLrAnjPuQ11wlgr6JkJaDvrIM6GqaIxk%2FdIln8J6cqES8KOotyZ06Z43ZTvCW0Qqq%2BvcBJAluNG%2FOlYb2ma6eqxg5p7bh%2BFbQ%3D&amp;pvid=12_115.205.168.149_2', '', '0.00', '/Public/Uploads/pic/2017-09-22/59c47c2227092.jpg', 1506049058, 1506009600, 1506614400, 'gadzook旗舰店', 0, 0, 1, 0, 1, 1, 0, ''),
(18, '男春秋2017新款秋季青年韩版潮流修身帅气男生秋装运动薄夹克', 2, '268.00', '79.00', '2.9', 'tmall', 558147648121, 'https://detail.tmall.com/item.htm?id=558147648121', 'https://s.click.taobao.com/t?spm=a311n.8189758/a.90200000.1.78174cfbqGzigI&amp;e=m%3D2%26s%3D4%2FEBQwDA535w4vFB6t2Z2ueEDrYVVa64K7Vc7tFgwiHjf2vlNIV67uwe%2FiDIUS0bIfNVgZHErC7LrAnjPuQ11wlgr6JkJaDvrIM6GqaIxk89TkDjCtZmaPmZekVEQHMu5p6uLoJe%2Fci4%2FApvI96NZgH8Jr', 'https://uland.taobao.com/coupon/edetail?e=2X%2BRtK3tIvSbhUsf2ayXDCufA857WRNcFTLqPpndZsfk5HJ74Rn%2BYWzbfL9YYAq%2BBc0%2BCU%2Bwei9fOuBBb5tKVAPbfjxGQb3QoUC34ijeiBtQMSY9kxCLFH1bHPcQ3U0WSRYGd14EbI7rSJ7ppANh9cIA0wgOB67qgPRfTgnhrZM%3D&amp;pid=mm_35638571_4680704_', '10.00', '/Public/Uploads/pic/2017-09-22/59c4e0f020149.jpg', 1506074864, 1506009600, 1506614400, 'tideace服饰旗舰店', 0, 0, 1, 2, 1, 1, 0, '12'),
(19, '男士修身毛线衣青年韩版套头毛衣男时尚打底衫', 2, '118.00', '38.00', '3.2', 'tmall', 557629885367, 'https://detail.tmall.com/item.htm?id=557629885367', 'https://detail.tmall.com/item.htm?id=557629885367', '', '0.00', 'http://img1.tbcdn.cn/tfscom/i1/1776940282/TB2akwXad.LL1JjSZFEXXcVmXXa_!!1776940282.jpg', 1506694394, 1506614400, 1506873600, 'gadzook旗舰店', 0, 6, 0, 2, 0, 0, 0, 'gadzook'),
(20, '122', 7, '22.00', '22.00', '10.0', 'taobao', 22, '222', '22', '111', '11.00', '22', 1507882751, 1507910400, 1508083200, '11', 0, 0, 1, 2, 1, 0, 0, '1'),
(21, '南极人男士保暖内衣男加厚加绒女冬秋衣', 2, '59.00', '39.90', '6.8', 'mall', NULL, '', NULL, NULL, '0.00', 'http://img2.tbcdn.cn/tfscom/i4/1579812499/TB1ydFtSVXXXXbWXXXXXXXXXXXX_!!0-item_pic.jpg', 1508585517, 1508428800, 1509206400, 'admin', 0, 0, 1, 2, 1, 1, 0, '秒杀'),
(26, '201622', 1, '11.00', '1.00', '0.9', 'mall', NULL, '', NULL, NULL, '0.00', '11', 1508651188, 1508601600, 1508688000, 'admin', 8, 0, 1, 2, 0, 1, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `think_goods_order`
--

DROP TABLE IF EXISTS `think_goods_order`;
CREATE TABLE IF NOT EXISTS `think_goods_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `gid` int(10) unsigned NOT NULL,
  `number` smallint(5) NOT NULL,
  `pay` decimal(10,2) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `think_goods_order`
--

INSERT INTO `think_goods_order` (`id`, `uid`, `gid`, `number`, `pay`, `status`, `addtime`) VALUES
(22, 6, 21, 1, '10.00', 1, 1508644485),
(21, 6, 21, 1, '10.00', 1, 1508644358),
(20, 6, 21, 1, '10.00', 1, 1508644114),
(19, 6, 21, 1, '10.00', 1, 1508644106),
(18, 6, 21, 1, '10.00', 1, 1508643999),
(17, 6, 21, 1, '10.00', 1, 1508641228),
(16, 6, 21, 1, '10.00', 1, 1508641107),
(15, 6, 21, 1, '10.00', 1, 1508599643),
(14, 6, 21, 1, '10.00', 1, 1508597014),
(13, 6, 21, 1, '10.00', 1, 1508596133),
(23, 6, 21, 1, '10.00', 1, 1508645859),
(24, 6, 21, 1, '10.00', 1, 1508656965),
(25, 6, 21, 1, '10.00', 1, 1508656970),
(26, 6, 21, 1, '10.00', 1, 1508657101);

-- --------------------------------------------------------

--
-- 表的结构 `think_link`
--

DROP TABLE IF EXISTS `think_link`;
CREATE TABLE IF NOT EXISTS `think_link` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `think_link`
--

INSERT INTO `think_link` (`id`, `title`, `url`, `desc`, `sort`) VALUES
(6, '淘宝网', 'http://www.taobao.com/', '淘宝', 1),
(7, '百度', 'http://www.baidu.com/', '百度搜索', 2),
(8, '京东', 'http://www.jd.com/', '京东商城', 3),
(11, '天猫', 'http://www.tmall.com', '天猫商城', 0),
(12, '新浪微博122', 'http://www.weibo.com', '微博', 6),
(13, 'LAYUI', 'http://www.layui.com/', 'LAYUI官网', 8),
(14, 'php', 'http://www.php.com', 'php', 9);

-- --------------------------------------------------------

--
-- 表的结构 `think_msg`
--

DROP TABLE IF EXISTS `think_msg`;
CREATE TABLE IF NOT EXISTS `think_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuid` int(11) DEFAULT NULL,
  `fname` varchar(51) DEFAULT NULL,
  `tuid` int(11) DEFAULT '0',
  `tname` varchar(50) DEFAULT 'SYSTEM',
  `content` text,
  `status` tinyint(1) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- 转存表中的数据 `think_msg`
--

INSERT INTO `think_msg` (`id`, `fuid`, `fname`, `tuid`, `tname`, `content`, `status`, `add_time`, `parent_id`) VALUES
(2, 5, 'admin', 0, 'SYSTEM', '签到不了', 1, 1506429988, 0),
(3, 5, 'admin', 0, 'SYSTEM', '电话联系方式是什么', 1, 1506430045, 0),
(18, 0, 'SYSTEM', 5, 'admin', '回复内容', 0, 1506526010, 1),
(22, 0, 'SYSTEM', 6, 'lingda', '我的layui给你发消息了', 0, 1507453634, 0),
(16, 0, 'SYSTEM', 6, 'lingda', '这个是发送给lindga的个人信件', 0, 1506525626, 0),
(20, 0, 'SYSTEM', -1, 'ALLUSER', '这个是发送全站内容', 0, 1506526087, 0),
(21, 6, 'lingda', 0, 'SYSTEM', '第一次发送信件', 1, 1506526807, 0),
(23, 0, 'SYSTEM', 7, 'darron', '我在layui给你发消息', 0, 1507453711, 0),
(24, 0, 'SYSTEM', 6, 'lingda', '回复邮件layui', 0, 1507454291, 21),
(25, 0, 'SYSTEM', 0, 'SYSTEM', '已经恢复正常', 1, 1507868592, 2),
(26, 0, 'SYSTEM', 6, 'lingda', '欢迎注册本站', 0, 1507868615, 0),
(27, 6, 'lingda', 0, 'SYSTEM', 'I found u', 1, 1507957412, 0),
(28, 6, 'lingda', 0, 'SYSTEM', 'I am boy', 1, 1507957949, 0),
(32, 0, 'SYSTEM', 6, 'lingda', 'YES, I KNOW.', 0, 1507958917, 28);

-- --------------------------------------------------------

--
-- 表的结构 `think_score_item`
--

DROP TABLE IF EXISTS `think_score_item`;
CREATE TABLE IF NOT EXISTS `think_score_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cateid` smallint(4) unsigned NOT NULL,
  `title` varchar(120) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `inventory` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `limit_num` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `num_iid` varchar(11) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `info` text,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) DEFAULT NULL COMMENT '兑换开始时间',
  `endtime` int(10) DEFAULT NULL COMMENT '兑换结束时间',
  `addtime` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `think_score_item`
--

INSERT INTO `think_score_item` (`id`, `cateid`, `title`, `pic`, `score`, `inventory`, `limit_num`, `num_iid`, `price`, `info`, `sort`, `starttime`, `endtime`, `addtime`) VALUES
(2, 1, '112 男女防蓝光树脂 时尚黑框老花眼镜50度600度', '/Public/Uploads/pic/2017-09-28/59cd086b3dcd4.jpg', 590, 42, 2, '55619741388', '59.00', '&lt;p&gt;&lt;img src=&quot;https://img.alicdn.com/imgextra/i3/1723946197/TB2BwcUc77myKJjSZFgXXcT9XXa_!!1723946197.jpg&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;https://img.alicdn.com/imgextra/i3/1723946197/TB2ziGMbKZkyKJjSszgXXcpMpXa_!!1723946197.jpg&quot;/&gt;&lt;/p&gt;', 0, 1506528000, 1507132800, 1506609259);

-- --------------------------------------------------------

--
-- 表的结构 `think_score_item_cate`
--

DROP TABLE IF EXISTS `think_score_item_cate`;
CREATE TABLE IF NOT EXISTS `think_score_item_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `think_score_item_cate`
--

INSERT INTO `think_score_item_cate` (`id`, `name`, `status`) VALUES
(1, '兑换商品', 1),
(2, '手机话费', 1),
(3, '支付宝红包', 1);

-- --------------------------------------------------------

--
-- 表的结构 `think_score_log`
--

DROP TABLE IF EXISTS `think_score_log`;
CREATE TABLE IF NOT EXISTS `think_score_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `score` int(10) NOT NULL,
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `think_score_log`
--

INSERT INTO `think_score_log` (`id`, `uid`, `uname`, `action`, `score`, `add_time`) VALUES
(5, 6, 'lingda', '兑换商品', 590, 1506864595);

-- --------------------------------------------------------

--
-- 表的结构 `think_score_order`
--

DROP TABLE IF EXISTS `think_score_order`;
CREATE TABLE IF NOT EXISTS `think_score_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `uname` varchar(20) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_num` mediumint(8) NOT NULL,
  `realname` varchar(25) NOT NULL DEFAULT '',
  `mobile` varchar(13) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `order_score` int(10) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ems_name` varchar(32) DEFAULT NULL,
  `ems_id` int(11) DEFAULT NULL,
  `add_time` int(10) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `think_score_order`
--

INSERT INTO `think_score_order` (`id`, `uid`, `uname`, `item_id`, `item_name`, `item_num`, `realname`, `mobile`, `address`, `order_score`, `status`, `ems_name`, `ems_id`, `add_time`, `remark`) VALUES
(18, 6, 'lingda', 2, '老花眼镜 男女防蓝光树脂 时尚黑框老花眼镜50度600度', 1, 'linda', '138888', '广西', 590, 1, 'EMS', 110911, 1506864595, '兑换商品');

-- --------------------------------------------------------

--
-- 表的结构 `think_sign`
--

DROP TABLE IF EXISTS `think_sign`;
CREATE TABLE IF NOT EXISTS `think_sign` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL,
  `last_date` int(10) NOT NULL DEFAULT '0',
  `sign_count` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `think_sign`
--

INSERT INTO `think_sign` (`id`, `uid`, `username`, `last_date`, `sign_count`) VALUES
(20, 6, 'lingda', 1507824000, 2);

-- --------------------------------------------------------

--
-- 表的结构 `think_sign_log`
--

DROP TABLE IF EXISTS `think_sign_log`;
CREATE TABLE IF NOT EXISTS `think_sign_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL,
  `sign_date` int(10) NOT NULL DEFAULT '0',
  `score` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- 转存表中的数据 `think_sign_log`
--

INSERT INTO `think_sign_log` (`id`, `uid`, `username`, `sign_date`, `score`) VALUES
(15, 6, 'lingda', 1507047671, 5),
(14, 6, 'lingda', 1507046977, 5),
(13, 6, 'lingda', 1507006371, 5),
(12, 6, 'lingda', 1507004249, 5),
(16, 6, 'lingda', 1507047850, 5),
(17, 6, 'lingda', 1507047992, 5),
(18, 6, 'lingda', 1507048163, 5),
(19, 6, 'lingda', 1507078859, 5),
(20, 6, 'lingda', 1507256687, 5),
(21, 6, 'lingda', 1507899043, 5),
(22, 6, 'lingda', 1507903673, 5),
(23, 6, 'lingda', 1507903814, 5),
(24, 6, 'lingda', 1507903858, 5),
(25, 6, 'lingda', 1507904248, 5);

-- --------------------------------------------------------

--
-- 表的结构 `think_user`
--

DROP TABLE IF EXISTS `think_user`;
CREATE TABLE IF NOT EXISTS `think_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uc_uid` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '0',
  `password` varchar(32) NOT NULL DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0' COMMENT '1男，0女',
  `intro` varchar(500) NOT NULL,
  `byear` smallint(4) unsigned NOT NULL,
  `bmonth` tinyint(2) unsigned NOT NULL,
  `bday` tinyint(2) unsigned NOT NULL,
  `province` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_level` int(10) unsigned NOT NULL DEFAULT '0',
  `avatar` varchar(255) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `realname` varchar(25) DEFAULT NULL,
  `mobile` varchar(13) DEFAULT NULL,
  `reg_ip` varchar(15) NOT NULL,
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0',
  `last_time` int(10) unsigned NOT NULL DEFAULT '0',
  `sign_time` int(10) DEFAULT '0',
  `last_ip` varchar(15) DEFAULT '0',
  `login_count` int(10) NOT NULL DEFAULT '1',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `think_user`
--

INSERT INTO `think_user` (`id`, `uc_uid`, `username`, `password`, `email`, `gender`, `intro`, `byear`, `bmonth`, `bday`, `province`, `city`, `score`, `score_level`, `avatar`, `qq`, `address`, `realname`, `mobile`, `reg_ip`, `reg_time`, `last_time`, `sign_time`, `last_ip`, `login_count`, `parent_id`, `status`) VALUES
(5, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@qq.com', 0, '', 0, 0, 0, '', '', 0, 0, '/Public/Uploads/pic/2017-09-27/59cb6782ce441.jpg', '123', 'gxgg', 'Mr.L', '1380000', '127.0.0.1', 1506403920, 1507008331, 0, '127.0.0.1', 15, 0, 1),
(6, 0, 'lingda', 'e10adc3949ba59abbe56e057f20f883e', '928242318@qq.com', 0, '', 0, 0, 0, '', '', 950, 0, '/Public/Uploads/pic/2017-10-14/59e1ad43144a7.jpg', '201612', '广西', 'linda', '186', '127.0.0.1', 1506524806, 1508989698, 0, '127.0.0.1', 49, 0, 1),
(7, 0, 'darron', 'e10adc3949ba59abbe56e057f20f883e', '505761730@qq.com', 0, '', 0, 0, 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', 1507008280, 0, 0, '0', 1, 5, 1);

-- --------------------------------------------------------

--
-- 表的结构 `think_webconf`
--

DROP TABLE IF EXISTS `think_webconf`;
CREATE TABLE IF NOT EXISTS `think_webconf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(32) DEFAULT NULL,
  `open` tinyint(1) NOT NULL DEFAULT '1',
  `logo` varchar(100) DEFAULT NULL,
  `qq` varchar(32) DEFAULT NULL,
  `beian` varchar(32) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `cnzz` text,
  `appkey` int(11) DEFAULT NULL COMMENT '淘宝appkey',
  `secretkey` varchar(60) DEFAULT NULL COMMENT '淘宝secretKey',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `think_webconf`
--

INSERT INTO `think_webconf` (`id`, `url`, `open`, `logo`, `qq`, `beian`, `title`, `keywords`, `description`, `cnzz`, `appkey`, `secretkey`) VALUES
(1, 'http://www.lingxiaoda.com', 1, NULL, '928242318', '桂ICP001号', 'LingXiaoDa', 'thinkphp,lingda', '网站描述dalin', '&lt;script type=&quot;text/javascript&quot;&gt;var cnzz_protocol = ((&quot;https:&quot; == document.location.protocol) ? &quot; https://&quot; : &quot; http://&quot;);document.write(unescape(&quot;%3Cspan id=''cnzz_stat_icon_5860079''%3E%3C/span%3E%3Cscript src=''&quot; + cnzz_protocol + &quot;s5.cnzz.com/stat.php%3Fid%3D5860079%26show%3Dpic'' type=''text/javascript''%3E%3C/script%3E&quot;));&lt;/script&gt;', 21440377, '0bf4536c8366ea50a8da93d7ca2c4f8e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
