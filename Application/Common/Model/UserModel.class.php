<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class UserModel extends BaseModel  {
        /*
         * 自动验证表单
         * 登录的时候无需验证username已经存在（重复），所以登录页面单独使用($_POST,4)做验证
         */
        protected $_validate = array(
            array('username','require','用户名不能为空！',0,'regex',3),
            array('username','','此用户名已经存在！',0,'unique',3),
            array('email','','此邮箱已经注册过会员！',0,'unique',1),//新增数据时候才验证
            array('password','require','密码不能为空！',0,'regex',3),
            array('password','6,20','密码长度6-20位！',0,'length',3),
            array('repassword','require','请确认密码！',0,'regex',3),
            array('repassword','password','确认密码不正确',0,'confirm',3), // 验证确认密码是否和密码一致
            array('verify','check_verify','验证码不正确！',0,'callback',3),
            array('username','require','用户名不能为空！',1,'regex',4),
            array('password','require','密码不能为空！',1,'regex',4),
            array('verify','check_verify','验证码不正确！',0,'callback',4),
            array('agreement', 'require', '请先同意本站服务使用协议！', 0), // 判断是否勾选网站安全协议
        );
        //自动完成
        protected $_auto = array (
            array('password','',2,'ignore'),//留空则忽略
            array('password','md5',3,'function') , // 对password字段在新增和编辑的时候使md5函数处理
            array('reg_time','time',1,'function'), // 字段在新增的时候写入当前时间戳
            array('reg_ip', 'get_client_ip', 1, 'function'), // 对regip字段在新增的时候写入当前注册ip地址

        );
        /*
         * Admin模块调用的方法
         */
        public function delUserData(){
            if($this-> where('id='.I('id'))->setField('status',0)){
                return array(
                    'status' => 'success',
                    'info' => '删除成功！',
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '删除失败！',
                );
            }
        }
        /*
         * Home模块调用的方法
         */
        public function login(){
            $password=$this->password;
            $info=$this->where(array('username'=>$this->username))->find();
            if($info){
                if($info['password']==$password){
                    session('id',$info['id']);
                    session('username',$info['username']);
                    $info['last_time']=time();
                    $info['last_ip']=get_client_ip();
                    $info['login_count']=$info['login_count']+1;
                    $this->where(array('id'=>$info['id']))->save($info);
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        public function editUserData(){
            $info=$this->find(I('id'));
            $old_password=I('password');
            if($info['password']==md5($old_password)){
                $result=$this->editData();
                return array(
                    'status' => $result['status'],
                    'info' => $result['info'],
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '网站登录密码不正确！',
                );
            }
        }
        public function resetPassWord(){
            $info=$this->find(I('id'));
            $old_password=I('old_password');
            if($info['password']==md5($old_password)){
                $result=$this->editData();
                return array(
                    'status' => $result['status'],
                    'info' => $result['info'],
                );
            }else{
                return array(
                    'status' => 'error',
                    'info' => '原网站登录密码不正确！',
                );
            }
        }
        public function setAvatar(){
            $data = I('post.');
            $result = uploadPic('pic');
            if($result['file']){
                $data['avatar'] =$result['file'];
            }else{
                return array(
                    'status' => 'error',
                    'info' => $result['error'],
                );
            }
            if($this->create($data)) {
                if($this->where('id=' . $data['id'])->setField('avatar', $data['avatar'])){
                    return array(
                        'status' => 'success',
                        'info' => '头像设置成功',
                    );
                }else{
                    return array(
                        'status' => 'error',
                        'info' => '头像设置失败',
                    );
                }
            }else{
                return array(
                    'status' => 'error',
                    'info' => $this->getError(),
                );
            }
        }

}