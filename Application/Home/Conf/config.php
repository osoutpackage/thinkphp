<?php
return array(
	//'配置项'=>'配置值'
    'TMPL_PARSE_STRING'  =>array(
        '__HOME_STATIC__' => __ROOT__ . trim(TMPL_PATH, '.') . 'Home/Public/static',
        '__STATIC__' => __ROOT__ . trim(TMPL_PATH, '.') . 'Home/Public/static',
    ),
    'SESSION_OPTIONS'         =>  array(
        'prefix'              =>  'home_',                     // 设置前缀 避免跟ADMIN模块冲突
        'expire'              =>  2*60*60,                        //SESSION保存时间
        'use_trans_sid'       =>  1,                               //跨页传递
        //'use_only_cookies'    =>  0,                               //是否只开启基于cookies的session的会话方式
    ),
    'TMPL_ACTION_SUCCESS'	=> 'public/success',  //设置success和error跳转模板
    'TMPL_ACTION_ERROR'		=> 'public/error',
);