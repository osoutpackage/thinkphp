<?php
namespace Home\Controller;
use Common\Controller\HomeBaseController;

class UserBaseController extends HomeBaseController   {
    /*
     * 设置未登录状态允许访问的控制器和方法
     * 用strpos判断当前操作是否在未登录状态允许访问
     * //strpos必须三个等号，当出现在第一个位置时返回一个0，也是true可以访问，不是false
     */
    function __construct() {
        parent::__construct();
        $this_action = CONTROLLER_NAME . "/" . ACTION_NAME;
        $login_action = "User/login,User/register,User/verify,Gift/index";
        //php的版本过低，如果使用了类似if(empty($a)){},这样的语句系统报错，php5.5以上正常
        //if (empty(session('id')) && strpos($login_action, $this_action) === false) {
        if (session('id')=='' && strpos($login_action, $this_action) === false) {
            $this->error('请先登录',U('User/login'));
        }else{
            if(session('id')){
                $User=M('User');
                $info=$User->where(array('id'=>session('id')))->find();
                $this->assign('info',$info);
            }
        }
    }


}