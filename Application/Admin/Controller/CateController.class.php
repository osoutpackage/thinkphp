<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class CateController extends AdminBaseController  {
    protected $_model='Cate';
    public function index(){
        $cate=D('Cate');
        $cate_res=$cate->getTree($where='',$order='sort asc');
        $this->assign('data',$cate_res);
        $this->display();
    }
    public function add(){
        $cate=D('Cate');
        if(IS_POST){
            $data=$cate->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate_res=$cate->getTree($where='',$order='sort asc');
            $this->assign('cateres',$cate_res);
        }
        $this->display();
    }
    public function edit(){
        $cate=D('Cate');
        if(IS_POST){
            $data=$cate->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate_res=$cate->find(I('id'));//得到一个分类的内容而已
            $this->assign('data',$cate_res);
            $cate_list=$cate->getTree($where='',$order='sort asc');
            $this->assign('cate_list',$cate_list);
        }
        $this->display();
    }
    /*
        public function del_all(){
            $ids_arr=I('id');
            if($ids_arr){
                $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
                $cate=D('cate');
                if($cate->delete($ids)){
                    $this->success("批量删除商品分类成功");
                }else{
                    $this->error("批量删除商品分类失败");
                }
            }else{
                $this->error("未选择有数据");
            }
        }

        public function sort(){
            $cate=D('cate');
            foreach ($_POST as $id=>$sort){
                if(is_int($id)){
                    $cate->where(array('id'=>$id))->setField('sort',$sort);
                }

            }
            $this->success("排序成功");
            //var_dump($_POST);Array ( [1] => 1 [2] => 2 [3] => 3 [5] => 4 )$id=>$sort
        }*/

}