<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class WebconfModel extends BaseModel  {
    public function saveConf(){
        $data=I('post.');
        $result = uploadPic('logo');
        if($result['file']){
            $data['logo'] =$result['file'];
        }else{
            $data['logo']=$conf_res['logo'];//原来的logo
        }
        if($this->create($data)){
            if(false!==$this->save()){
                if(self::setPhp($data)){
                    return array(
                        'status' => 'success',
                        'info' => '设置成功',
                    );
                }else{
                    return array(
                        'status' => 'error',
                        'info' => '无法写入Webconf.php',
                    );
                }
            }else{
                return array(
                    'status' => 'error',
                    'info' => '设置失败',
                );
            }
        }else{
            return array(
                'status' => 'error',
                'info' => $this->getError(),
            );
        }
    }
    public function setPhp($data){
        $path = COMMON_PATH .'Conf/webconf.php';
        $config = array(
            'WEB_URL' => I('url'),
            'WEB_NAME' => I('title'),
            'WEB_LOGO' => $data['logo'],
            'WEB_QQ' => I('qq'),
            'WEB_BEIAN' => I('beian'),
            'WEB_KEYWORD' => I('keywords'),
            'WEB_DESC' => I('description'),
            'WEB_CNZZ' => I('cnzz'),
            'WEB_APPKEY' => I('appkey'),
            'WEB_SECRETKEY' => I('secretkey'),
        );
        $str = '<?php return array(
        ';
        foreach ($config as $key => $value){
            $str .= '"'.$key.'"'.'=>'.'"'.$value.'"'.',
            ';
        };
        $str .= '); ?>';
        if(file_put_contents($path, $str)){
            return true;
        }else {
            return false;
        }

    }

}