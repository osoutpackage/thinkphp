<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class ScoreItemController extends AdminBaseController  {
    protected $_model='ScoreItem';
    public function index(){
        $article = D('ScoreItemView'); // 实例化对象
        $data = $article->getPageData($where='',$order='sort desc');
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function add(){
        if(IS_POST){
            $item=D('ScoreItem');
            $data = $item->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate=D('ScoreItemCate');
            $cate_res=$cate->select();
            $this->assign('cateres',$cate_res);
        }
        $this->display();
    }
    public function edit(){
        $item=D('ScoreItem');
        if(IS_POST){
            $data = $item->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate=D('ScoreItemCate');
            $cateres=$cate->select();
            $itemres=$item->find(I('id'));
            $assign=array(
                'cateres'=>$cateres,
                'data'=>$itemres,
            );
            $this->assign($assign);
        }

        $this->display();
    }

    public function sort(){
        $item=D('ScoreItem');
        foreach ($_POST as $id=>$sort){
            $item->where(array('id'=>$id))->setField('sort',$sort);
        }
        $this->success("更新排序成功");
        //var_dump($_POST);
    }

}