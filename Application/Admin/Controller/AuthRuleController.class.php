<?php
/*
 * 操作权限列表
 */
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class AuthRuleController extends AdminBaseController
{
    protected $_model='AuthRule';
    /* 分类树
     * @$where
     * @is_menu =0功能菜单，is_menu =1是管理员菜单 这里需要获取所有权限列表
     */
    public function index(){
        $auth_rule = D('AuthRule');
        $list=$auth_rule->getTree($where='',$order='sort asc');
        $this->assign('data',$list);
        $this->display();
    }
    public function add(){
        $auth_rule=D('AuthRule');
        if(IS_POST){
            $data=$auth_rule->addData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }else{
            $list=$auth_rule->getTree($where='is_menu = 1',$order='sort asc');
            $this->assign('menu_list',$list);
        }
        $this->display();
    }
    public function addchild(){
        $auth_rule=D('AuthRule');
        if(IS_POST){
            $data=$auth_rule->addData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }else{
            //通过get获取父id
            $data['id']=I('id');
            $this->assign('data',$data);
        }
        $this->display();
    }
    public function edit(){
        $auth_rule=D('AuthRule');
        if(IS_POST){
            $data=$auth_rule->editData();
            $this->ajaxReturn(array(
                'status' => $data['status'],
                'info' => $data['info'],
            ));
        }else{
            $auth_rule_res=$auth_rule->find(I('id'));
            $list=$auth_rule->getTree($where='is_menu = 1',$order='sort asc');
            $assign=array(
                'data'=>$auth_rule_res,
                'menu_list'=>$list,
            );
            $this->assign($assign);
        }
        $this->display();
    }


}