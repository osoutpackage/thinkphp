<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Home\Controller;
use Think\Controller;

class ForgetController extends Controller
{
    public function index(){
        $this->display();
    }
    public function send_email(){
        $user = M('User');
        if(IS_POST){
            $condition['email'] = I('mail');
            $find_user=$user->where($condition)->find();
            if($find_user){
                $address=I('mail');
                $subject="亲爱的会员，您正在使用邮箱找回密码";
                $token = md5($find_user['username'].$find_user['password']);
                $url = C('WEB_URL').U('Forget/reset')."?email=" . $address . "&token=" . $token;
                $content="亲爱的会员：<br/>您提交了找回密码请求。请点击下面的链接重置密码（2小时内有效）。<br/><a href='" . $url . "' target='_blank'>" . $url . "</a><br/>如果以上链接无法点击，请将它复制到你的浏览器地址栏中进入访问。<br/>如果您没有提交找回密码请求，请忽略此邮件。";
                session('reset_'.$find_user['id'],time());
                $result=sendEmail($address,$subject,$content);
                if($result['status']=='success'){
                    $this->ajaxReturn(array(
                        'status' => 'success'
                    ));
                }else{
                    $this->ajaxReturn(array(
                        'status' => 'error'
                    ));
                }
            }else{
                $this->ajaxReturn(array(
                    'status' => 'error'
                ));
            }
        }
        $this->display('index');
    }

    public function reset(){
        $user = M('User');
        if(IS_POST){
            $password=md5(I('password'));
            if($user->where('id='.I('id'))->setField('password',$password)){
                session('reset_'.I('id'),null);
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }else{
            $condition['email'] = I('email');
            $find_user=$user->where($condition)->find();
            $token = md5($find_user['username'].$find_user['password']);
            if(session('reset_'.$find_user['id'])){
                if($token==I('token')){
                    $this->assign('id',$find_user['id']);
                    $this->display('reset');//显示修改密码的表单
                }else{
                    $this->error('无效链接');
                }
            }else{
                $this->error('该链接已经过期');
            }
        }
    }
}