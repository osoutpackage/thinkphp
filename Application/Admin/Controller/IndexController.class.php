<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;

class IndexController extends AdminBaseController {
    public function index(){
        $this->display();
    }
    public function main(){
        $this->display();
    }
    public function delcache()
    {
        if(IS_POST){
            $path = "./Runtime/";
            $data = I('data');
            $count = count($data);
            for ($i = 0; $i < $count; $i++) {
                $this->dirDel($path . $data[$i] . "/");
            }
            $this->success("清除成功");
        }else{
            $this->display();
        }

    }
    /* is_dir() 函数检查指定的文件是否是目录。
    *  opendir() 函数打开目录句柄。
    *  readdir() 函数返回目录中下一个文件的文件名。
    *  目录的所有文件并去掉 . 和 .. 上下级目录
    *  rmdir() 函数删除空的目录。
    */
    public function dirDel($path)
    {
        if (!is_dir($path)) {
            //$this->error($path . "已经清理过了！");
            return;
        }
        $hand = opendir($path);
        while (($file = readdir($hand)) !== false) {
            if ($file == "." || $file == "..") {
                continue;
            }
            if (is_dir($path . "/" . $file)) {
                $this->dirDel($path . "/" . $file);
            } else {
                @unlink($path . "/" . $file);
            }
        }
        closedir($hand);
        @rmdir($path);
    }
}