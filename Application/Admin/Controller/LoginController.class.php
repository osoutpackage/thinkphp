<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller {
    public function index(){
        $admin=D('Admin');//相当于$admin = new \Home\Model\AdminModel();
        if(IS_POST){
            if($admin->create($_POST,4)){
                if($admin->login()){
                    $this->ajaxReturn(array(
                        'status' => 'success',
                        'info' => '登录成功！',
                        'callback' => U('Index/index'),
                    ));
                }else{
                    $this->ajaxReturn(array(
                        'status' => 'error',
                        'info' => '用户名或者密码不正确！',
                    ));
                }
            }else{
                $this->ajaxReturn(array(
                    'status' => 'error',
                    'info' => $admin->getError(),
                ));
            }
            return;
        }
        if(session('id')){
            //echo session('id');
            $this->error("您已经登录过",U('Index/index'));
        }else{
            $this->display('Login/index');
        }

    }
    public function verify(){
        $Verify = new \Think\Verify();
        $Verify->fontSize = 18;
        $Verify->codeSet = '0123456789';
        $Verify->length = 4;
        $Verify->useNoise = false;
        $Verify->entry();
    }
}