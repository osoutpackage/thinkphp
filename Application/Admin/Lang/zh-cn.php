<?php

/**
 * 这个是Admin模块语言包 仅限Admin模块引用的，其他模块无法使用。
 * lingda的 简体中文语言包
 */
return array(
    /* 语言变量 */
    'admin_success_action'     => '操作成功',
    'admin_error_action'         => '操作失败',


);
