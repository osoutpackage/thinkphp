<?php

namespace Common\Controller;
use Think\Controller;

class HomeBaseController extends Controller
{
    public function _initialize(){
        //parent::_initialize();
        if(session('id')){
            $visitor['id']=session('id');
            $visitor['username']=session('username');
            $this->assign('visitor',$visitor);
        }
        //初始化缓存
        $cache = S(array('prefix'=>'home_','expire'=>3600));
        if($cache->cate_data){
            $cate_res = $cache->cate_data;
        }else{
            $cate=D('Cate');
            $cate_res = $cate->getAllData($where='pid=0',$order='sort asc');
            $cache->cate_data = $cate_res;
        }
        if($cache->link_data){
            $link_list = $cache->link_data;
        }else{
            $link=D('Link');
            $link_list=$link->getAllData($where='',$order='sort desc');
            $cache->link_data = $link_list;
        }
        $assign=array(
            'cateres'=>$cate_res,
            'link_list'=>$link_list,
        );
        $this->assign($assign);

        /*
         * 初始化缓存，设置缓存数据
         */
        /*
        $cache = S(array('type'=>'File','prefix'=>'home','expire'=>3600));
        if($cache->cate_data){
            $cate_res = $cache->cate_data;
        }else{
            $cate=D('Cate');
            $cate_res = $cate->getAllData($where='pid=0',$order='sort asc');
            $cache->cate_data = $cate_res;
        }
        if($cache->link_data){
            $link_list = $cache->link_data;
        }else{
            $link=D('Link');
            $link_list=$link->getAllData($where='',$order='sort desc');
            $cache->link_data = $link_list;
        }
        $assign=array(
            'cateres'=>$cate_res,
            'link_list'=>$link_list,
        );
        $this->assign($assign);*/
        /*
         * 设置title
         */
        switch (CONTROLLER_NAME)
        {
            case "Index":
                $title = C('WEB_NAME');
                break;
            case "Gift":
                $title ='积分商城 - '.C('WEB_NAME');
                break;
            case 'User':
                $title ='会员中心 - '.C('WEB_NAME');
                break;
            case 'Baoming':
                $title ='商家合作 - '.C('WEB_NAME');
                break;
            case 'Help':
                $title ='帮助中心 - '.C('WEB_NAME');
                break;
            case 'Forget':
                $title ='找回密码 - '.C('WEB_NAME');
                break;
            default:
                $title = C('WEB_NAME');
        }
        $this->assign('title',$title);
    }
}