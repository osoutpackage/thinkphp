<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
use Common\Helper\Redis;
class MallController extends AdminBaseController  {
    protected $_model='Goods';
    public function index(){
        $goods = D('GoodsView'); // 实例化对象
        $where['status'] = 2;//$where = array('status'=>2);
        $where['site'] = 'mall';
        $data = $goods->getGoodsData($where,$order='id desc');
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function add(){
        if(IS_POST){
            $goods=D('Goods');
            if($goods->create()){
                $gid=$goods->add();
                if($gid){
                    //设置redis
                    $redis = new Redis();
                    $key = 'gid_'.$gid;
                    $count = I('number');
                    for($i=1;$i<=$count;$i++){
                        $redis->rPush($key,1);//在指定Key所关联的List Value的尾部插入参数中给出的所有Values
                    }
                    $expiration = strtotime(I('endtime')) - time();
                    $redis->setExpire($key,$expiration);//设置缓存过期时间
                    $this->success('添加成功',U('index'));
                }else{
                    $this->error('添加失败');
                }
            }else{
                $this->error($goods->getError());
            }
            return;
        }
        $cate=D('Cate');
        $cate_res=$cate->getTree($where='',$order='sort asc');
        $this->assign('cateres',$cate_res);
        $this->display();
    }
    public function edit(){
        $goods=D('Goods');
        if(IS_POST){
            $data = $goods->editData();
            if($data['status']=='success'){
                //设置redis
                $redis = new Redis();
                $key = 'gid_'.I('id');
                $count = I('number');
                $redis->rm($key);//编辑状态先删除旧的再追加key value
                for($i=1;$i<=$count;$i++){
                    $redis->rPush($key,1);//在指定Key所关联的List Value的尾部插入参数中给出的所有Values
                }
                $expiration = strtotime(I('endtime')) - time();
                $redis->setExpire($key,$expiration);//设置缓存过期时间
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $goods_res=$goods->find(I('id'));
            $cate=D('Cate');
            $cate_res=$cate->getTree($where='',$order='sort asc');
            $assign=array(
                'goods'=>$goods_res,
                'cateres'=>$cate_res,
            );
            $this->assign($assign);
        }
        $this->display();
    }
    public function del(){
        $goods=D('Goods');
        if($goods->delete(I('id'))){
            $redis = new Redis();
            $key = 'gid_'.I('id');
            $redis->rm($key);//删除缓存
            $this->ajaxReturn(array(
                'status' => 'success',
                'info' => '删除成功！',
            ));
        }else{
            $this->ajaxReturn(array(
                'status' => 'error',
                'info' => '删除失败！',
            ));
        }
    }
    /*
     * 批量删除商品
     */
    public function bDel(){
        $ids_arr=I('id');
        if($ids_arr){
            $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
            $goods=D('Goods');
            if($goods->delete($ids)){
                /*
                 * 批量删除缓存
                 */
                $redis = new Redis();
                foreach ($ids_arr as $value) {
                    $redis->rm('gid_'.$value);
                }
                $this->success('批量删除成功',U('index'));
            }else{
                $this->error('批量删除失败');
            }
        }else{
            $this->error('未选择有数据');
        }
    }





}