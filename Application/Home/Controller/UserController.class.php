<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends UserBaseController  {
    public function index(){
        $this->display();
    }
    public function register(){
        if(IS_POST){
            $user=D('User');
            $data = $user->addData();
            if($data['status']=='success'){
                $this->success('注册成功，请登录！',U('User/login'));
            }else{
                $this->error('注册失败');
            }
            return;
        }else{
            if(session('id')){
                $this->error("您已经登录",U('User/index'));
            }else{
                $this->assign('parent_id',session('parent_id'));
                $this->display('register');
            }
        }
    }
    public function login(){

        if(IS_POST){
            $user=D('User');
            if($user->create($_POST,4)){
                if($user->login()){
                    $this->success('登录成功！',U('User/index'));
                }else{
                    $this->error('用户名或者密码不正确');
                }
            }else{
                $this->error($user->getError());
            }
            return;
        }else{
            if(session('id')){
                $this->error("您已经登录过",U('User/index'));
            }else{
                $this->display('login');
            }
        }
    }
    public function logout(){
        session('id',null);
        session('username',null);
        $this->success("退出成功",U('User/login'));
    }

    public function info(){
        if(IS_POST){
            $user=D('User');
            $data = $user->editUserData();
            if($data['status']=='success'){
                $this->success($data['info'],U('User/index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $this->display();
    }
    public function password(){
        if(IS_POST){
            $user=D('User');
            $data=$user->resetPassWord();
            if($data['status']=='success'){
                $this->success($data['info'],U('User/index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $this->display();
    }
    public function msg(){
        $do=I('do');
        if($do=='in'){
            $msg=D('Msg');
            $where['tuid'] = array(array('eq',-1),array('eq',session('id')), 'or') ;
            $data=$msg->getPageData($where);
            $assign=array(
                'msg_list'=>$data['list'],
                'page'=>$data['page'],
            );
            $this->assign($assign);
            $this->display('msg_in');
        }elseif($do=='out'){
            $msg=D('Msg');
            $where['fuid'] = session('id');
            $data=$msg->getPageData($where);
            $assign=array(
                'msg_list'=>$data['list'],
                'page'=>$data['page'],
            );
            $this->assign($assign);
            $this->display('msg_out');
        }elseif($do=='del'){
            $ids_arr=I('ids');
            if($ids_arr){
                $ids=implode(',',$ids_arr);//把id数组转化为逗号连接的数据传入delete删除
                $msg=M('Msg');
                if($msg->delete($ids)){
                    $this->success("批量删除成功");
                }else{
                    $this->error("批量删除失败");
                }
            }else{
                $this->error("未选择有数据");
            }

        }else{
            if(IS_POST){
                $msg=D('Msg');
                $data=I('post.');
                $data['fuid']=session('id');
                $data['fname']=session('username');
                $data=$msg->addData($data);
                if($data['status']=='success'){
                    $this->success($data['info'],U('index'));
                }else{
                    $this->error($data['info']);
                }
                return;
            }else{
                $this->display('msg_send');
            }
        }
    }
    public function union(){
        $union_id= session('id');
        $union_url='invite='.$union_id;
        $union_url=base64_encode($union_url);//加密方法
        $this->assign('union_url',$union_url);
        $this->display();
    }
    public function unionlist(){
        $union_list=D('User');
        $where['parent_id'] = session('id');
        $data=$union_list->getPageData($where);
        $assign=array(
            'union_list'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function avatar(){
        $User = D("User");
        if(IS_POST) {
            $data=$User->setAvatar();
            if($data['status']=='success'){
                $this->success($data['info']);
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $this->display();
    }
    public function gift(){
        $oder=D('ScoreOrder');
        $where['uid'] = session('id');
        $data=$oder->getPageData($where);
        $assign=array(
            'order_list'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function mingxi(){
        $action=D('SignLog');
        $where['uid'] = session('id');
        $data=$action->getPageData($where);
        $assign=array(
            'logs_list'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function verify(){
        $Verify = new \Think\Verify();
        $Verify->fontSize = 18;
        $Verify->length = 4;
        $Verify->useNoise = false;
        $Verify->entry();
    }
}