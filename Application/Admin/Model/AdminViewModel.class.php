<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Model;
use Think\Model\ViewModel;
class AdminViewModel extends ViewModel {
    public $viewFields = array(
        'Admin'=>array('id','username','login_time','_type'=>'LEFT'),//用户表
        'AuthGroupAccess'=>array('uid','group_id','_on'=>'Admin.id=AuthGroupAccess.uid','_type'=>'LEFT'),//用户和角色的关系表
        'AuthGroup'=>array('title','_on'=>'AuthGroupAccess.group_id=AuthGroup.id'),//角色表

    );
    public function getAdminData(){
        $count = $this->count();
        $Page = new \Think\Page($count,20);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->order('id asc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }


}