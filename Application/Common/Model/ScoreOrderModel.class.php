<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class ScoreOrderModel extends BaseModel
{
    protected $_validate = array(
        //发货行为
        array('ems_name','require','快递公司不能为空！',0,'regex',3),
        array('ems_id','require','快递单号不能为空！',0,'regex',3),
        //兑换商品行为
        array('realname','require','姓名不能为空！',0,'regex',1),
        array('mobile','require','手机号码不能为空！',0,'regex',1),
        array('address','require','收货地址不能为空！',0,'regex',1),
    );
    protected $_auto = array (
        array('add_time','time',1,'function'),//兑换商品行为
    );
    /*
     * 检测用户积分是否足够
     */
    public function check_score(){
        $user=M('User');
        $order_score=$this->order_score;
        $condition['id']=$this->uid;
        $user_score=$user->where($condition)->getField('score');
        if($user_score > $order_score){
            return true;
        }else{
            return false;
        }
    }
    /*
     * 检测是否兑换超限
     */
    public function  check_limit_num(){
        $order_num=$this->item_num;
        $condition['uid']=$this->uid;
        $condition['item_id']=$this->item_id;
        $have_order=$this->where($condition)->find();
        if($have_order){
            return false;
        }else{
            $item=M('ScoreItem');
            $item_res = $item->where('id='.$condition['item_id'])->find();
            if(($order_num < $item_res['limit_num'])&&($order_num < $item_res['inventory'])){
                return true;
            }else{
                return false;
            }
        }
    }

    /*
     * 减用户积分，减兑换商品库存
     */
    public function deduct(){
        $user=M('User');
        $order_score=$this->order_score;
        $condition['id']=$this->uid;
        if($user->where($condition)->setDec('score',$order_score)){//减用户积分
            $item=M('ScoreItem');
            $where['id']=$this->item_id;
            $order_num=$this->item_num;
            if ($item->where($where)->setDec('inventory',$order_num)){//减少兑换相应库存
                $action=D('ScoreLog');//写入事件
                if($action->create()) {
                    $action->score = $this->order_score;
                    $action->uname = $this->uname;
                    $action->action = '兑换商品';
                    $action->add();
                }
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}