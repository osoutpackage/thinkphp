<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Model;
use Common\Model\BaseModel;

class AuthRuleModel extends BaseModel  {
    protected $_validate = array(
        array('title','require','权限名称不能为空！',0,'regex',3), // 必须有
        array('name','','该权限已经存在！',0,'unique',3), // 验证是否已经存在
    );
    protected $_auto = array (

        array('is_menu','getIS_Menu',3,'callback'),
    );
    public function getIS_Menu(){
        if(I('is_menu')){
            $is_menu=1;
        }else{
            $is_menu=0;//为空的时候设置为0，否则会被认为不修改。
        }
        return $is_menu;
    }
    /*
    //分类树 is_menu =0所有菜单，is_menu =1是管理员菜单
    public function getTree($is_menu=0){
        if($is_menu==1){
            $where['is_menu']=1;
        }
        $data=$this->where($where)->order("sort asc")->select();
        return parent::toLevel($data);
    }
    public function getMenu(){
        $where['is_menu']=1;
        $data=$this->where($where)->order("sort asc")->select();
        return parent::toLayer($data);
    }

    //递归对所有分类重新排序 一维数组
    public function resort($data,$parent_id=0,$level=0){
        static $ret=array();
        foreach ($data as $k=>$v){
            if($v['pid']==$parent_id){
                $v['level']=$level;
                $ret[]=$v;
                $this->resort($data,$v['id'],$level+1);
                //print_r($level."--".$v['id']."</br>");
            }
        }
        return $ret;
    }
    */





}