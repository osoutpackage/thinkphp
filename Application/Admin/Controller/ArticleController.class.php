<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class ArticleController extends AdminBaseController  {
    protected $_model='Article';
    public function index(){
        $article = D('ArticleView'); // 实例化对象
        $data = $article->getArticleData();
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function add(){
        if(IS_POST){
            $article = D('Article');
            $data = $article->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $cate=D('ArticleCate');
            $cate_res=$cate->getAllData();
            $this->assign('cate',$cate_res);
        }

        $this->display();
    }
    public function edit(){
        $article=D('Article');
        if(IS_POST){
            $data = $article->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $article_res=$article->find(I('id'));
            $cate=D('ArticleCate');
            $cate_res=$cate->getAllData();
            $assign=array(
                'article'=>$article_res,
                'cate'=>$cate_res,
            );
            $this->assign($assign);
        }

        $this->display();
    }


    public function search(){
        $article = D('ArticleView'); // 实例化对象
        $keywords=I('keywords');
        if(I('cateid')){
            $condition['cateid'] = I('cateid');
            $this->assign('article_cateid',$condition['cateid']);
        }
        $condition['title'] = array('like','%'.$keywords.'%');
        $count = $article->where($condition)->count();
        $Page = new \Think\Page($count,10);
        //分页跳转的时候保证查询条件
        foreach($condition as $key=>$val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        //$list = $article->order('sort desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        $list = $article->where($condition)->order('sort desc')->page($_GET['p'].',10')->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach($list as $key=>$value ){
            $list[$key]['time']=date("Y-m-d", $value['time']);
        }
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $article_cate=M('ArticleCate');
        $article_cateres=$article_cate->order('sort asc')->select();
        $this->assign('article_cateres',$article_cateres);
        $this->display('lst'); // 输出模板
    }



}