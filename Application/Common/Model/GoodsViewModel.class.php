<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Think\Model\ViewModel;
class GoodsViewModel extends ViewModel {
    public $viewFields = array(
        'Goods'=>array('id','title','pic','starttime','endtime','price','promotion_price','sort','number','url','_type'=>'LEFT'),
        'Cate'=>array('catename','_on'=>'Goods.cateid=Cate.id'),
    );
    public function getGoodsData($where = 'status = 2',$order = 'sort desc'){
        $count = $this->where($where)->count();
        $Page = new \Think\Page($count,20);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->where($where)->order($order)->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }


}