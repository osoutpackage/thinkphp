
$(function() {
    var $navFun = function() {
        var st = $(document).scrollTop(), 
            headh = $("#fixednav").offset().top;           
            $nav_classify = $(".container");
			
			
        if(st > headh){
            $nav_classify.addClass("fixed");
			 $(".container").slideDown(500);
        } else {
            $nav_classify.removeClass("fixed");
			$(".container").slideUp(10);
			
        }
    };

    var F_nav_scroll = function () {
        if(navigator.userAgent.indexOf('iPad') > -1){
            return false;
        }      
        if (!window.XMLHttpRequest) {
           return;          
        }else{
            //默认执行一次
            $navFun();
            $(window).bind("scroll", $navFun);
        }
    }
    F_nav_scroll();
	
});
