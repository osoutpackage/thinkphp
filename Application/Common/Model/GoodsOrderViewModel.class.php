<?php
namespace Common\Model;
use Think\Model\ViewModel;
class GoodsOrderViewModel extends ViewModel  {
    public $viewFields = array(
        'GoodsOrder'=>array('id','uid','gid','number','pay','status','addtime','_type'=>'LEFT'),
        'Goods'=>array('title','pic','price','promotion_price','_on'=>'GoodsOrder.gid=Goods.id'),
        'User'=>array('username', '_on'=>'GoodsOrder.uid=User.id'),
    );
    public function getGoodsOrderData(){
        $count = $this->count();
        $Page = new \Think\Page($count,20);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->order('id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }
}