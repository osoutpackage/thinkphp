<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class SignLogModel extends BaseModel  {
        protected $_auto = array (
            array('add_time','time',1,'function'),
        );
        /*
         * 点击签到行为插入事件
         */
        public function addSignLogAction($data){
            $sign_data['uid']=$data['uid'];
            $sign_data['username']=$data['username'];
            $sign_data['sign_date']=strtotime(date("Y-m-d H:i:s",time()));
            $sign_data['score']=5;
            if($this->create($sign_data)){
                if($this->add()){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }


}