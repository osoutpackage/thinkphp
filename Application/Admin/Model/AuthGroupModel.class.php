<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Admin\Model;
use Common\Model\BaseModel;

class AuthGroupModel extends BaseModel  {
    protected $_validate = array(
        array('title','require','角色名称不能为空！',0,'regex',3), // 必须有
        array('title','','该角色已经存在！',0,'unique',3),
    );



}