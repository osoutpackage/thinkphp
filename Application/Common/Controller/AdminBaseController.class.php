<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Controller;
use Common\Controller\BaseController;
class AdminBaseController extends BaseController {
    public function __construct()
    {
        parent::__construct();
        if(!session('id')){
            if(IS_AJAX){
                $this->ajaxReturn(array(
                    'status' => 'error',
                    'info' => '请先登录后再操作',
                ));
            }else{
                $this->error('请先登录',U('Login/index'));
            }

        }
        /*
         * 权限控制
         * 需要验证的规则列表
         * 用户的id
         */

        $auth=new \Think\Auth();
        $rule_name=CONTROLLER_NAME.'/'.ACTION_NAME;
        $result=$auth->check($rule_name,session('id'));
        if(!$result){
            if(IS_AJAX){
                $this->ajaxReturn(array(
                    'status' => 'error',
                    'info' => '没有权限！',
                ));
            }else{
                $this->error('没有权限!');
            }
        }
        // 分配菜单数据
        $menu_data=D('AuthRule')->getMenu();
        //p($menu_data);
        $this->assign('menu_data',$menu_data);

    }


}