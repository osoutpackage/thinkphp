<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 * 管理员角色
 */

namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class AuthGroupController extends AdminBaseController
{
    protected $_model='AuthGroup';


    /*
     * 分配权限列表
     * 获取权限列表树结构
     * 查找角色已有的权限rules以便前台页面展现勾选状态
     * explode打散rules数组，前台利用in_array判断
     * @I('name')的value对应权限列表里的id值
     */
    public function giverule(){
        $auth_group = D('AuthGroup');
        if(IS_POST){
            $rule_ids = implode(",", I('name'));
            if(!count($rule_ids)){
                $this->error('请选择需要分配的权限');
            }
            $data['id']=I('role_id');
            $data['rules']=$rule_ids;
            if(false!==$auth_group->save($data)){
                $this->success('分配成功', U('AuthGroup/index'));
            }else{
                $this->error('分配失败');
            }
            return;
        }else{
            $auth_rule = D('AuthRule');
            $list=$auth_rule->getTree($where='',$order='sort asc');
            $this->assign('data',$list);
            $role_info = $auth_group->find(I('role_id'));
            if($role_info['rules']){
                $rulesArr = explode(',',$role_info['rules']);
                $this->assign('rulesArr',$rulesArr);
            }
        }
        $this->display();
    }

}