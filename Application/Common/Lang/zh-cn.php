<?php

/**
 * 这个是应用公共语言包 所有模块公用的
 * 控制器里使用L('success_action')方法使用，模块文件中使用{$Think.lang.success_action}或者｛:L('success_action')｝引用
 * lingda的 简体中文语言包 应用公共语言包
 */
return array(
    /* 语言变量 */
    'success_action'     => '操作成功',
    'error_action'         => '操作失败',


);
