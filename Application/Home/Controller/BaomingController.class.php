<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 * Date: 2017/9/29 13:52
 */

namespace Home\Controller;


class BaomingController extends UserBaseController
{
    public function index(){

        $this->display();
    }
    public function add(){
        if(IS_POST) {
            $goods=D('goods');
            $data = $goods->addData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }
        $cate=M('cate');
        $cateres=$cate->where('pid=0')->select();
        $this->assign('cateres',$cateres);
        $this->display();
    }
    public function my(){
        $goods = D('Goods'); // 实例化Data数据对象
        $where=array('seller_id'=>session('id'));
        $data=$goods->getPageData($where,$order='');
        $assign=array(
            'data'=>$data['list'],
            'page'=>$data['page'],
        );
        $this->assign($assign);
        $this->display();
    }
    public function edit(){
        $goods = D('Goods');
        if(IS_POST){
            $data = $goods->editData();
            if($data['status']=='success'){
                $this->success($data['info'],U('index'));
            }else{
                $this->error($data['info']);
            }
            return;
        }else{
            $goodsres=$goods->find(I('id'));
            $cate=M('cate');
            $cateres=$cate->where('parent_id=0')->select();
            $assign=array(
                'goods'=>$goodsres,
                'cateres'=>$cateres,
            );
            $this->assign($assign);
        }

        $this->display(); // 输出模板
    }
    public function del(){
        $goods=D('goods');
        if($goods->delete(I('id'))){
            $this->success("商品删除成功");
        }else{
            $this->error("商品删除失败");
        }
    }
    public function shenhe(){
        $this->display();
    }
    public function yaoqiu(){
        $this->display();
    }

}