<?php
namespace Common\Model;
use Common\Model\BaseModel;

class GoodsModel extends BaseModel  {
    protected $_validate = array(
        array('title','require','商品名称不能为空！',0,'regex',3),
        array('url','require','商品链接不能为空！',0,'regex',3),
        array('num_iid','require','商品id不能为空！',0,'regex',3),
        array('cateid','require','商品分类不能为空！',0,'regex',3),
        array('price','require','商品原价不能为空！',0,'regex',3),
        array('promotion_price','require','商品促销价不能为空！',0,'regex',3),
        array('pic','require','商品图片不能为空！',0,'regex',1),//1新增数据时候验证
        array('starttime','require','请设置活动开始时间！',0,'regex',3),
        array('endtime','require','请设置活动结束时间！',0,'regex',3),

    );
    protected $_auto = array (
        array('addtime','time',1,'function'),
        array('starttime','strtotime',3,'function'),
        array('endtime','strtotime',3,'function'),
        array('discount','getDiscount',3,'callback'),
        array('ispost','getIspost',3,'callback'),
        array('recom','getRecom',3,'callback'),
    );
    public function getDiscount(){
        $discount=round(I('promotion_price')/I('price'),2)*10;
        return $discount;
    }
    public function getIspost(){
        if(I('ispost')){
            $ispost=1;
        }else{
            $ispost=0;//为空的时候设置为0，否则会被认为不修改。
        }
        return $ispost;
    }
    public function getRecom(){
        if(I('recom')){
            $recom=1;
        }else{
            $recom=0;//为空的时候设置为0，否则会被认为不修改。
        }
        return $recom;
    }
    /*
     * 如果商品图采取上传到服务器的话，需要写一个delete前置操作把服务器本地的图片删除掉
     */
    //$pic_url= $goods->where('id='.I('id'))->getField('pic');
    //@unlink('.'.$pic_url);//如何是上传图片需要删除服务器图片
    public function uploadGoods(){
        $upload_result = uploadFile('excel');
        if(!$upload_result['file']){
            return array(
                'status' => 'error',
                'info' => $upload_result['error'],
            );
        }
        $excel_data=importExcel($upload_result['file']);
        //p($excel_data);die();
        //$excel_data=import_excel('./Upload/excel/test-2017-10-18.xls');
        $item['cateid'] = I('cateid');
        $add_count = $edit_count = 0;
        foreach ($excel_data as $key => $value){
            $item['num_iid'] = $value[0];
            $item['title'] = $value[1];
            $item['pic'] = $value[2];
            $item['url'] = $value[3];
            if(strpos($item['url'],'taobao.com') !== false){
                $item['site']='taobao';
            }else{
                $item['site']='tmall';
            }
            $item['promotion_price'] = $value[5];
            $item['price'] = $value[5];
            $item['starttime'] = $value[12];
            $item['endtime'] = $value[13];
            $item['nick'] = $value[14];
            $item['tk_url'] = $value[15];
            $item['nick'] = $value[14];
            //p($item);die();
            $isExist = $this->where('num_iid='.$item['num_iid'])->getField('id');
            if($isExist){
                $item['id'] = $isExist;
                $result=$this -> editData($item);
                if($result['status']=='success'){
                    $edit_count++;
                }
            }else{
                $result=$this -> addData($item);
                if($result['status']=='success'){
                    $add_count++;
                }
            }
        }
        @unlink($upload_result['file']);//删除上传的excel文件
        return array(
            'status' => 'success',
            'info' => "成功导入".$add_count."个商品; 更新".$edit_count."个商品",
        );
    }
    public function getGoodsData($where='',$order='sort desc',$perPage=20,$cache=false){
        if($cache){
            //初始化缓存
            $cache = S(array('prefix'=>'home_','expire'=>3600));
            $p = I('p')?I('p'):0;
            $cate = I('cateid')?I('cateid'):0;
            $goods_key = 'goods_'.$cate.'_'.$p.'_data';
            $count_key = 'goods_'.$cate.'_'.$p.'_count';
            if($cache->$goods_key){
                return array(
                    'list' => $cache->$goods_key,
                    'page' => $cache->$count_key,
                );
            }
        }

        $count = $this->where($where)->count();
        $Page = new \Think\Page($count,$perPage);//$Page=new \Org\Nx\Page($count,$perpage);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->where($where)->order($order)->limit($Page->firstRow.','.$Page->listRows)->select();
        if($cache){
            $cache->$goods_key = $list;
            $cache->$count_key = $show;
        }
        return array(
            'list' => $list,
            'page' => $show,
        );
    }

}