<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class ScoreItemModel extends BaseModel  {
    protected $_validate = array(
        array('title','require','商品名称不能为空！',0,'regex',3),
        array('cateid','require','商品分类不能为空！',0,'regex',3),
        array('price','require','商品价格不能为空！',0,'regex',3),
        array('pic','require','商品图片不能为空！',0,'regex',1),//1新增数据时候验证
        array('starttime','require','请设置开始时间！',0,'regex',3),
        array('endtime','require','请设置结束时间！',0,'regex',3),

    );
    //自动完成
    protected $_auto = array (
        array('addtime','time',1,'function'), // 字段在新增的时候写入当前时间戳
        array('starttime','strtotime',3,'function'),
        array('endtime','strtotime',3,'function'),

    );


}