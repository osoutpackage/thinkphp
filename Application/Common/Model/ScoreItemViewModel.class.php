<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Think\Model\ViewModel;
class ScoreItemViewModel extends ViewModel {
    public $viewFields = array(
        'ScoreItem'=>array('id','title','pic','starttime','endtime','price','score','inventory','sort','_type'=>'LEFT'),
        'ScoreItemCate'=>array('name','_on'=>'ScoreItem.cateid=ScoreItemCate.id'),
    );
    public function getPageData($where='',$order=''){
        $count = $this->where($where)->count();
        $Page = new \Think\Page($count,20);
        $show  = $Page->show();// 分页显示输出
        // 进行分页数据查询
        $list = $this->where($where)->order($order)->limit($Page->firstRow.','.$Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }


}