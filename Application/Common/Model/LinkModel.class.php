<?php
/**
 * Created by PhpStorm.
 * Author: lingda (928242318@qq.com)
 */

namespace Common\Model;
use Common\Model\BaseModel;

class LinkModel extends BaseModel  {
        protected $_validate = array(
        array('title','require','链接名称不能为空！',0,'regex',3), // title必须
        array('url','require','链接名称不能为空！',0,'regex',3), // 必须
        array('title','','链接名称不得重复！',0,'unique',3), // 验证是否已经存在
    );



}