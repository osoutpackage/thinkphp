<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/28
 * Time: 10:47
 */

namespace Home\Controller;
use Think\Controller;
use Common\Helper\Redis;
class BuyController extends UserBaseController
{

    protected $_msg = '';
    public function index(){
        $this->display();
    }
    public function buy_now(){
        $id = I('id');
        $redis = new Redis();
        $key = 'gid_'.$id;
        //判断是否还有库存
        $count 	= $redis->lPop($key);
        if(!$count){
            $this->error('商品已经下架');
        }else{
            //插入并记录秒杀用户的请求,购买信息插入
            $uid=session('id');
            $order_key='buy_gid_'.$id.'_uid_'.$uid;
            $redis->rPush($order_key,$uid);
        }
        /*
        * 创建订单
        */
        sleep(mt_rand(0,5));//延迟插入数据库
        $order_uid = $redis->lPop($order_key);
        $order = D('GoodsOrder');
        if($order->addOrder($order_uid)){
            $this->success('下单成功',U('index'));
        }
        /*
         * 异步处理创建订单
         * 可以使用定时任务开启处理
         */
        /*
         $uid=session('id');
         $order_key='buy_gid_'.$id;
         $redis->rPush($order_key,$uid);//秒杀阶段把所有秒杀成功的用户放进$order_key里
        //定时任务
        $order_key='buy_gid_'.$id;//把秒杀商品的id传进入
        $order_key_count=$redis->lLen($order_key);
         for($i=1;$i<=$order_key_count;$i++){
            $order_uid = $redis->lPop($order_key);//返回并弹出指定Key关联的链表中的第一个元素，即头部元素
            if($this->addOrder($order_uid)){
                 $this->success('下单成功',U('index'));
            }
        }
         */

        /*
         * 同步处理创建订单
         */
        /*
        $order = D('Order');
        $data = array('uid'=>6,'gid'=>21,'number'=>1,'pay'=>10,'status'=>1);
        if ($order->addData($data)){
            if(D('Goods')->where('id='.$id)->setDec('number',1)){
                $this->success('下单成功',U('index'));
            }else{
                $this->error('系统出错');
            }
        }else{
            $this->error('系统繁忙亲稍后再试');
        }*/
        $this->display();
    }


}