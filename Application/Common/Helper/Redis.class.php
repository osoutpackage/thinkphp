<?php
namespace Common\Helper;

class Redis {
    /*
     * 长连接是指多次请求之间可以对redis连接进行复用，即只在第一次执行请求是建立连接，以后每次请求只是从连接池中将连接取出，不再重新建立连接；而短连接表示连接在多次请求之间不可复用，每次请求都需要重新建立连接。
     */
    public function __construct($options=array()) {
        $options = array_merge(array (
            'host'          => C('REDIS_HOST') ? : '127.0.0.1',
            'port'          => C('REDIS_PORT') ? : 6379,
            'timeout'       => C('DATA_CACHE_TIMEOUT') ? : false,
            'persistent'    => false,
        ),$options);
        $this->options =  $options;
        $this->options['expire'] =  isset($options['expire'])?  $options['expire']  :   C('DATA_CACHE_TIME');
        $this->options['prefix'] =  isset($options['prefix'])?  $options['prefix']  :   C('DATA_CACHE_PREFIX');
        $this->options['length'] =  isset($options['length'])?  $options['length']  :   0;
        $func = $options['persistent'] ? 'pconnect' : 'connect';
        $this->handler  = new \Redis;
        $options['timeout'] === false ?
            $this->handler->$func($options['host'], $options['port']) :
            $this->handler->$func($options['host'], $options['port'], $options['timeout']);

    }

    public function rPush($key,$value){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->rPush($key,$value);//rPush尾部插入参数中给出的Value
    }
    public function lPush($key,$value){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->lPush($key,$value);//在指定Key所关联的List Value的头部插入参数中给出的所有Values
    }
    public function lLen($key){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->lLen($key);//返回指定Key关联的链表中元素的数量
    }
    public function lPop($key){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->lPop($key);//new \Redis -> lpop返回并弹出指定Key关联的链表中的第一个元素，即头部元素
    }
    /*
     * 如果所有给定 key 都不存在或包含空列表，那么 BLPOP 命令将阻塞连接，直到等待超时
     * 或有另一个客户端对给定 key 的任意一个执行 LPUSH 或 RPUSH 命令为止。
     */
    public function blPop($key,$timeout=0){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->blPop($key,$timeout);//new \Redis -> lpop返回并弹出指定Key关联的链表中的第一个元素，即头部元素
    }
    public function rPop($key){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->rPop($key);//返回并弹出指定Key关联的链表中的最后一个元素，即尾部元素
    }
    /**
     * 写入缓存
     * @access public
     * @param string $key 缓存变量名
     * @param mixed $value  存储数据
     * @param integer $expire  有效时间（秒）
     * @return boolean
     */
    public function set($key, $value, $expire = null) {
        if(is_null($expire)) {
            $expire  =  $this->options['expire'];
        }
        $key   =   $this->options['prefix'].$key;
        //对数组/对象数据进行缓存处理，保证数据完整性
        $value  =  (is_object($value) || is_array($value)) ? json_encode($value) : $value;
        if(is_int($expire) && $expire) {
            $result = $this->handler->setex($key, $expire, $value);
        }else{
            $result = $this->handler->set($key, $value);
        }
        return $result;
    }
    /**
     * 读取缓存
     * @access public
     * @param string $name 缓存变量名
     * @return mixed
     */
    public function get($key) {
        $value = $this->handler->get($this->options['prefix'].$key);
        $jsonData  = json_decode( $value, true );
        return ($jsonData === NULL) ? $value : $jsonData;	//检测是否为JSON数据 true 返回JSON解析数组, false返回源数据
    }
    /*
     * 设置缓存时间
     * @param string $key  缓存键
     * @param integer $expiration  有效时间（秒）
     */
    public function setExpire($key,$expiration){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->expire($key,$expiration);
    }
    /*
     * 查看剩余生存时间
     * @param string $key  缓存键
     */
    public function getExpire($key){
        $key   =   $this->options['prefix'].$key;
        return $this->handler->ttl($key);
    }
    /**
     * 删除缓存
     * @access public
     * @param string $name 缓存变量名
     * @return boolean
     */
    public function rm($key) {
        $key   =   $this->options['prefix'].$key;
        return $this->handler->del($key);
    }
    /**
     * 清除缓存
     * @access public
     * @return boolean
     */
    public function clear() {
        return $this->handler->flushDB();
    }


}

?>