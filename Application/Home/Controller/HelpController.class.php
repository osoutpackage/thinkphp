<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/28
 * Time: 10:47
 */

namespace Home\Controller;
use Common\Controller\HomeBaseController;

class HelpController extends HomeBaseController
{

    public function read(){
        $article_cate=M('ArticleCate');
        $article_cateres=$article_cate->order('sort asc')->select();
        $article=M('Article');
        $article_res = $article->where('cateid='.I('id'))->find();
        $article_res['content']=htmlspecialchars_decode($article_res['content']);
        $assign=array(
            'read_id'=>I('id'),
            'help_list'=>$article_cateres,
            'article'=>$article_res,
        );
        $this->assign($assign);
        $this->display();
    }
    public function qianggou(){
        $this->display();
    }
    public function xiaobao(){
        $this->display();
    }

}